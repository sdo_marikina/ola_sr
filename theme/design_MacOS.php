<style type="text/css">
	/* width */
::-webkit-scrollbar {
    width: 2px;
    background: rgba(0,0,0,0.1); 
}
.sidebar ::-webkit-scrollbar {
    width: 2px;
    background: rgba(0,0,0,0.1); 
}
.homerow .card{
	background-color: rgba(255,255,255,0.5);
}
.notificationpanel-header{
	padding: 15px;
}
.hoverableitem{
	transition: all 0.3s;

}
.btn-special{
	border-radius: 20px !important;
	background-color: rgba(255,255,255,0.03) !important;
	color: white;
}
.btn-special:hover{
	border-radius: 20px;
	box-shadow: 0px 15px 20px rgba(0,0,0,0.5);
	background-color: white!important;
	transform: scale(1.1);
}
.hoverableitem:hover{
	cursor: pointer;
	box-shadow: 0px 5px 8px rgba(0,0,0,0.2);

}
.loading_indicator{
	display: block;
	top: 0;
	bottom: 0;
	left: 0;
	right: 0;
	position: absolute;
	height: 100%;
	width: 100%;
	z-index: 100;
	background-color: white;
	background: url(images/loading.gif);
	background-position: center;
	background-repeat: no-repeat;
	background-size: 150px auto;
	background-color: white;
	animation-name: poptop;
	animation-duration: 0.5s;
	display: none;
}
@keyframes OpenNotif{
0%{
margin-right: -400px;
}
100%{
margin-right: 0px;
}
}

.notificationpanel{
	animation: OpenNotif;
		animation-duration: 0.5s;
		transition: all 0.5s;
        margin: 0px;
	top: 0;
	bottom: 0;
	right: 0;
	position: fixed;
	z-index: 1000000;
	background-color: #34495e;
	box-shadow: 0px 2px 5px rgba(0,0,0,0.2);
	width: 400px;
	border: 1px solid rgba(255,255,255,0.2);
	border-radius: 0px;
	color: white;
	overflow: hidden;
	padding-left: 20px;
	padding-right: 20px;
}
.notificationpanel-body{
	padding: 10px;
}
.padder{
	padding-top: 80px;
	padding-bottom: 80px;
}

.reveal{
	animation-name: reveal;
	animation-duration: 1s;
}
    .wholebar{display: block; position: fixed; top: 0; bottom: 0; left: 0; right: 0; margin: 50px; margin-top: 80px;
    	margin-right: 100px;
    	margin-left: 100px;
       overflow: hidden;
       border-radius: 4px;
       /*box-shadow: 0px 0px 50px rgba(0,0,0,0.5);*/
    }
    .wholebar .wholebar_left{
        display: block;  position: relative; top: 0; bottom: 0; left: 0; width: 50%; height: 100%; float: left;
        overflow-x: hidden;
        overflow-y: auto;
    }
     .wholebar .wholebar_right{
        display: block;  position: relative; top: 0; bottom: 0;  right: 0; width: 50%; height: 100%; float: right;
       /*background-color: ;*/
       background-color: rgba(255,255,255,0.7);
        padding: 20px;
        /*overflow-x: hidden;*/
        /*overflow-y: auto;*/
        overflow: hidden;
    }
        .wholebar .wholebar_top{
        display: block;  position: relative; top: 0;  left: 0; right: 0; width: 100%; height: 50%; float: top;
       
        padding: 5px;
    }
     .wholebar .wholebar_bottom{
        display: block;  position: relative; bottom: 0; left: 0; right: 0; width: 100%; height: 50%; float: bottom;
       
        padding: 5px;
    }
@keyframes reveal{
0%{
opacity: 0.3;
transform: scale(0.9);
margin-bottom: -256px;
/*box-shadow: 0px 20px 50px gray;*/
/*padding:20px !important;*/
/*border-radius: 10px;*/
/*border: 1px solid black;*/
background-color: #bdc3c7 !important;
}
60%{
transform: scale(0.9);
margin-bottom: 0px;
/*box-shadow: 0px 20px 50px gray;*/
/*padding:20px !important;*/
/*border-radius: 10px;*/
/*border: 1px solid black;*/
background-color: #bdc3c7 !important;
}
100%{

}
}
.flipper{
animation-name: flipper;
animation-duration: 0.3s;
}
@keyframes flipper{
0%{

transform: rotateX(60deg);
}
100%{

}
}
.softslide{
animation-name: softslide;
animation-duration: 0.5s;
}
@keyframes softslide{
0%{

transform: scale(0.6) rotateY(60deg);
}
100%{

}
}
.bgworthy{
	background-size: cover;
	background-position: center;
	background-repeat: no-repeat;
	width: 100%;
padding:20px;
padding-top: 50px;
padding-bottom:50px;
	color: white;
	text-shadow: 1px 1px 1px black;
}
.modal .bgworthy{
	border-radius:  4px 4px 0px 0px;
}
  video, canvas {
    margin-left: 230px;
    margin-top: 120px;
    position: absolute;
  }
.card-link{
	color: #34495e;
}

.nav-link{
	border-radius: 0px !important;
}
.alert{
	border-radius: 0px;
	border: 1px solid rgba(0,0,0,0.1);
}
.nav-item{
	border-radius: 0px !important;
}
.backfill{
	  background: rgba(44, 62, 80,0.6);
	  display: block;
	  position: absolute;
	  top: 0;
	  right: 0;
	  bottom: 0;
	  left: 0;
	  height: 100%;
	  width: 100%;
}

		.cameraapp{
			border-radius: 15px !important;
			overflow: hidden;
			width: 320px;
			height: 240px;
		}
.lessen{
	opacity: 0.5;
}
/* Track */
::-webkit-scrollbar-track {
    background: rgba(0,0,0,0) !important; 
}
/* Handle */
::-webkit-scrollbar-thumb {
    background: #34495e; 
}

 .myautoscroll {
         /*   height: 40ex;
            width: 40em;*/
            overflow: hidden !important;
            /*border: 1px solid #444;*/
            /*margin: 3em;*/
            /*box-shadow: 0px 0px 20px rgba(0,0,0,0.2);*/
        }
        .myautoscroll:hover {
            overflow: auto !important;
            /*padding-right: 1px;*/
              box-shadow:none;
        }
        .myautoscroll p {
            padding-right: 0;
            background-color:#ff0;
        }
        .myautoscroll:hover p {
            padding-right: 0px;
        }
/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
    background: rgba(0,0,0,0) !important; 
}
.consistent_shadow{
	box-shadow: 0px 1px 2px rgba(0,0,0,0.2);
}
.sidebar{
	background-position: center !important;
	background-size: cover !important;
	background-attachment: fixed !important;
	top: 0;
	bottom: 0;
	padding: 20px;
	left: 0;
	background-color: #f1f2f6 !important;
	position: fixed;
	width: 256px;
	/*z-index: -1;*/
	display: block;
	margin-top: 57px;
	border-right: 1px solid #dfe4ea;
	padding-left: 0px;
	padding-right: 0px;
	/*z-index: 100;*/
	overflow: auto;

	/*font-family: sanfranc;*/
	/*box-shadow: 0px 20px 100px rgb(52, 152, 219,0.2);*/
}
.sidebar hr{
	border: none !important;
	/*border-bottom: 2px solid #ced6e0 !important;*/
	padding-top: 5px;
}
.badge{
	border-radius: 0px;
	font-weight: 0px;
}
.sidebar .sidebar_link{
	color: #747d8c;
	font-size: 17px;
	display: block;
	width: 95%;
	border-radius: 0px 3px 3px 0px;
	padding-left: 20px;
	padding-right: 20px;
	padding-top: 8px;
	padding-bottom: 8px;
	font-size: 16px;
	text-decoration: none;
	transition: 0.3s all;
	border-top: 1px solid transparent;
	border-bottom: 1px solid transparent;
}
.separator{
	border-top: 1px solid rgba(0,0,0,0.1) !important;
}
.sidebar .sidebar_title{
	padding-left: 20px;
	padding-right: 20px;
	color: #546E7A;
}
.sidebar .sidebar_link:hover{

color: #4a69bd !important;
}
.thumbimage{
	width: 100%;
	height: 100px;
	background-size: cover;
	background-repeat: no-repeat;
	border-radius: 4px;
	background-position: center;
	border: 2px solid #2c3e50;
}
.rightbar{
		position: fixed;
		top: 0;
		bottom: 0;
		right: 0;
		left: 280;
		width: 100%;
		padding-left: 256px;
		margin-top: 50px;
		background-color: white;
		display: block;
		z-index: -2;
		overflow-x:hidden;
		overflow-y:auto;
		animation-name: rbar;
		animation-duration: 0.5s;
}
.rightbar .navbar{
	background-color: white !important;
	font-family: sfcamera;
}
.rightbar .navbar .nav-link{
	color: #2c3e50 !important;
}
.rightbar .navbar .navbar-brand{
	color: #2c3e50 !important;
}
@keyframes rbar{
	0%{
		opacity: 0.5;
	}
}
/*INDEX*/
.poptop_anim{

animation-name: poptop !important;
animation-duration: 0.5s !important;

}

@keyframes poptop{
	0%{
		/*transform: scale(1.1);*/
		margin-top: 50px;
		opacity: 0;
	}
}
			.heightscale_anim{
animation-name: heightscale !important;
animation-duration: 0.5s !important;
overflow: hidden;
}
@keyframes heightscale{
	0%{
		transform: scale(0.8);
	}
}

.fadeinner{
			width:20%; height: 50px; margin:5px; 
			background-position: center;
			background-size: cover;
			animation-name: fader;
			animation-duration: 5s;
			display:block;
			float: right;
			transition: 5s all;
			border-radius: 2px;
		}
		.fadeinner_full{
			width: 80%; height: 100px; margin:5px; 
			background-position: center;
			background-size: cover;
			animation-name: fader;
			animation-duration: 5s;
			display:block;
			float: right;
			transition: 5s all;
			border-radius: 2px;
		}
		.c1{
			animation-name: put_contents;
			animation-duration: 1s;
		}
		@keyframes put_contents{
			0%{
				margin-top: 10px;
				transform: scale(1.1);
			}
		}
/*INDEX*/
@font-face{
	  font-family: 'sanfranc';
  src: url('theme/fonts/sanfrancisco.otf'); /* IE9 Compat Modes */
}
@font-face{
	  font-family: 'unarrow';
  src: url('theme/fonts/bebas.ttf'); /* IE9 Compat Modes */
}
@font-face{
	  font-family: 'sanfranc_black'; src: url('theme/fonts/sanfrancisco_black.ttf'); /* IE9 Compat Modes */
}
@font-face{
	  font-family: 'heavy'; src: url('theme/fonts/heavy.ttf'); /* IE9 Compat Modes */
}
@font-face{
	 font-family: 'sanfranc_thin'; src: url('theme/fonts/sf_thin.ttf'); /* IE9 Compat Modes */
}
@font-face{
	 font-family: 'sfcamera'; src: url('theme/fonts/sfcam.otf'); /* IE9 Compat Modes */
}

.ultranarrow{
	font-family: unarrow;
}
		.notifbox{
			position: fixed;
			padding: 10px;
			border-radius: 1px;
			background-color: rgba(236, 240, 241,1.0);
							box-shadow: 0px 1px 50px rgba(0,0,0,0.2);
			width: 400px;
			display: block;
			top: 0;
			right: 0;
			margin: 20px;
			margin-top:60px;
			z-index: 1000000000;
			transition: 4.8s all;
			animation-name: shownotif;
			animation-duration: 5s;
			display: none;
			border:none;
			border-radius: 3px;
			border: 1px solid rgba(0,0,0,0.1);
			perspective:1000px !important;

		}
@keyframes shownotif{
	0%{
		margin-right: -20px;
		opacity: 0;
	}
	10%{
		margin-right: 10px;
		opacity: 1;
		transform: scale(1);
	}
	90%{
		margin-right: 10px;
		opacity: 1;
		transform: scale(1);
	}
	100%{
		margin-right: -20px;
		opacity: 0;
		transform: scale(0.9);
	}
}
.lgmodal{
	top: 0;
	left: 0;
	right: 0;
	display: block;
	background-color: rgba(255,255,255,0.5);
	position: fixed;
	z-index: 100;
	height: 100%;
	width: 100%;
	overflow: auto;
	text-align: center;
	display: none;
}
.btn{
	border-radius: 0px;
	background-color: white;
	border: 0px;
	/*box-shadow: 0px 1px 2px rgba(0,0,0,0.2);*/
	border-top : 1px solid white;
	border-radius: 2px;
	overflow: hidden;
	border: 1px solid rgba(0,0,0,0.03);
	font-family: /*heavy*/;
}
.btn:hover{
	color: #2c3e50 !important;	
border-color: #2c3e50 !important;	
background-color: white;
}
	progress[value] {
  /* Reset the default appearance */
  -webkit-appearance: none;
   appearance: none;

  width: 250px;
  height: 20px;
}

progress[value]::-webkit-progress-bar {
  background-color: white;
  border: 1px solid #34495e;
  border-radius: 4px;
}

progress[value]::-webkit-progress-value {
 /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#2da0ff+0,0082ed+100 */
background: #34495e; /* Old browsers */
border-radius: 2px;
}
.btn-light{
color: #2c3e50 !important;	
border-color: #2c3e50 !important;
}
.btn-primary{
/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#2da0ff+0,0082ed+100 */
background-color: #2c3e50;
border-color: #2c3e50;
color: white;
}
.navbar .btn-primary{
/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#2da0ff+0,0082ed+100 */
background-color: transparent;
border-color: transparent;
/*color: white;*/
color: #2c3e50 !important;	
}
.btn-secondary{
color: #2c3e50 !important;	
border-color: #2c3e50 !important;	
}
.btn-danger{
color: #c0392b !important;	
border-color: #c0392b !important;
}
@keyframes scaler{
	0%{
		top: -10px;
		/*transform: scale(0.9);*/
	}
}

.dropdown-menu{
	border-radius: 4px;
	border: none;
	box-shadow: 0px 10px 50px rgba(0,0,0,0.4);
	animation-name: scaler;
	animation-duration: 0.2s;
}
.form-control{
	border: none !important;
	border-bottom: 1px solid rgba(52, 73, 94,0.5) !important;
	border-radius: 0px;
	background-color: transparent;
}
.form-control:focus{
	 outline:none !important;
}
.form-group label{
	color: #34495e;
}
nav .form-control{
border-radius: 0px;
border: 1px solid rgba(0,0,0,0.1);
}
.dropdown-menu a{
	padding: 12px;
}
.dropdown-menu a:hover{
	cursor: pointer !important;
	color: white !important;
background-color: #34495e;
}
.dropdown-item{
	padding: 12px !important;
}
.dropdown-item:hover{
	cursor: pointer !important;
	color: white !important;
background-color: #34495e !important;
}

.ultrabold{
	font-family:sanfranc_black;
}
.ultrathin{
	font-family:sanfranc_thin;
}
.ultratitle{
	font-family: sfcamera;
}
.lgmodal-close{
/*position: fixed;*/
				box-shadow: 0px 1px 2px rgba(0,0,0,0.2);
border:none;
border-radius: 0px 0px 10px 10px;
display: inline-block;
background-color: white;
}
.lgmodal-inner{
	background-color: white;
	height: 90%;
	width: 95%;
	display: block;
	margin: 0 auto;
	margin-top: 20px;

	margin-bottom: 50px;
					box-shadow: 0px 1px 2px rgba(0,0,0,0.2);
	border-radius: 10px;
	/*overflow: hidden;*/
}
button[disabled]{
  border: 1px solid #999999 !important;
  background-color: #cccccc !important;
  color: #666666 !important;
  opacity: 0.4 !important;
}



	body{
			font-family: sanfranc !important;
			overflow-x: hidden;
			margin-bottom: 100px;
			background-size: cover;
			background-attachment: fixed;
			animation-name: faderx !important;
			animation-duration: 1s !important;
	}
	@keyframes faderx{
		0%{
			opacity: 0.5 !important;
		}
	}
	
		.hasbg{
			 background-repeat: no-repeat !important;
			background-attachment: fixed !important;
			background-position: center !important;
			background-size: cover !important;
			text-shadow: 0px 20px 50px rgba(0,0,0,0.5) !important; 
			color: white !important;

		}
		.hasbg hr{
			border-top: 1px solid white !important;
		}
		.custom-select{
			padding-left: 20px !important;
			padding-right: 20px !important;
		}
		.blurbg{
			background-color: #2980b9;
			background-position: center !important;
			background-size: cover !important;
			background-attachment: fixed !important;
		}
		.blurbg_sub{
					background-color: #2c3e50;
			background-position: center !important;
			background-size: cover !important;
			background-attachment: fixed !important;
		}
		.xfooter{
			text-shadow: 0px 5px 30px rgba(0,0,0,0.3);
			color: #222 !important;
			position: fixed;
			left: 0;
			bottom: 0;
			width: 100%;
			font-size: 12px;
			padding: 10px;
			background-color: transparent;
		}
		.table{
			animation-name : opentable;
			animation-duration: 0.5s;
		}
		.card .table{
			animation-name : openwindow;
			animation-duration: 0.5s;
		}
		.card{
			border: 0px;
			border: 1px solid #CFD8DC;
				border-radius: 0px;
	background-color:white;
	
	/*box-shadow: 0px 1px 2px rgba(0,0,0,0.2);*/
	/*border-top : 1px solid white;*/
	border-radius: 3px;
	/*overflow: hidden;*/
	margin-bottom: 5px;
		}
		.card-header{
			border: none !important;
			border-radius: 0px !important;
		}
		h1{
			animation-name : slidetitle !important;
			animation-duration: 0.5s;
		}
		a{
			color: #03A9F4;
		}
		.majortext{
			color: black !important;
		}
		.jumbotron{
			background-color: white;
			background-position: bottom;
			background-size: cover;
			/*background-attachment: fixed;*/
			color: #34495e;
			border-radius: 0px;
			padding: 20px;
			padding-top: 30px;
			padding-bottom: 30px;
			/*text-shadow: 0px 0px 70px rgba(0,0,0,0.2);*/
		}
		.defaultbg{
			background-color: #ecf0f1;
			background-position: center;
			background-size: cover;
			background-attachment: fixed;
		}
			.navbar{
			background-color: #ecf0f1;
			background-position: center;
			background-size: cover;
			background-attachment: fixed;
			border-bottom: 1px solid #dfe4ea;
			border-top: none;
		}
		.modal{
			background-color: rgba(127, 140, 141,0.4);
		}
		.modal-content{
			/*overflow: hidden;*/
			background-color: #ecf0f1;
			background-position: center;
			background-size: cover;
			background-attachment: fixed;
			border: none;
			border-radius: 0px;
			box-shadow: 0px 1px 2px rgba(0,0,0,0.2);
			transition: 0.8s all !important;
			border-top : 1px solid rgba(255,255,255,0.1);
			border-radius: 4px;
			overflow: hidden;
			perspective: 1000px;

		}
		@keyframes modal_show{
			0%{
				opacity: 0;
				transform: scale(0.8);
			}
			60%{
				transform: scale(1);
			}
		}
		.modal-footer{
			border: none;
		}
		.modal-header .ptitle{
			width: 70%;

			font-size: 15px;
			text-align: center;
			width: 100%;
		}
		.modal-header .ptitle small{
			color: black;
		}
		.modal-header .ptitle img{
			float: left;
		}
		.labelinput{
			text-align: center; border: none; background-color: transparent !important;
		}



</style>

