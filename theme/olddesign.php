
<style type="text/css">
/* width */
::-webkit-scrollbar {
    width: 5px;
    background: rgba(0,0,0,0.1); 
}

/* Track */
::-webkit-scrollbar-track {
    background: rgba(0,0,0,0.1); 
}
.powerlook_resultbar{
	 width: 500px;
        height: 500px;
        background-color: white;
        position: fixed;
        border: 1px solid rgba(0,0,0,0.2);
        z-index: 5;
        margin-top: 279px;
        border-radius: 4px;
}
/* Handle */
::-webkit-scrollbar-thumb {
    background: #888; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
    background: rgba(0,0,0,0.1); 
}

	@font-face{
		 font-family: 'lucida';
	  src: url('theme/fonts/LucidaGrande.ttf'); /* IE9 Compat Modes */
	}
		body{
		font-family: lucida;
					animation-name : openbody !important;
			animation-duration: 0.6s;	
			overflow-x: hidden;	
	}
	@keyframes openbody{
			0%{
				opacity: 0.5;
			}

			100%{

			}
		}
		.form-control{
			padding-left: 15px;
			padding-right: 15px;
			padding-top: 1px;
			padding-bottom: 1px;
			font-size: 12px;
			height: 30px;
			border: 1px solid #999 ;
			border-bottom: : 1px solid rgba(0,0,0,0.01) !important;
			border-radius: 20px;
		}
		.btn-group-vertical .btn{
			min-width: 150px;
			border-radius: 4px;
			padding: 5px;
			text-align: left;
			white-space: normal;
		}
		.xi{
			border-radius: 4px;
			box-shadow: 0px 2px 10px rgba(0,0,0,0.5);
			border: 1px solid rgba(0,0,0,0.4);
			max-width: 70%;
			margin: 20px;
		}
		.btn-group-vertical{
			width: 100% !important;
		}
		.hasbg{
			 background-repeat: no-repeat !important;
			background-attachment: fixed !important;
			background-position: center !important;
			background-size: cover !important;
			text-shadow: 0px 20px 50px rgba(0,0,0,0.5) !important; 
			color: white !important;

		}
		.hasbg hr{
			border-top: 1px solid white !important;
		}
		.custom-select{
			padding-left: 20px !important;
			padding-right: 20px !important;
		}
		.jumbotron{
			background: #cccccc; /* Old browsers */
			color: black;
			text-shadow: 0px 1px 0px rgba(255,255,255,0.7);
		}
		.btn{
			font-size: 12px;
		    border-radius: 3px;
		    border: 1px solid #999;
		    padding: 0px;
						    /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#eeeeee+0,cccccc+100 */
			background: #eeeeee; /* Old browsers */
			background: -moz-linear-gradient(top, #eeeeee 0%, #cccccc 100%); /* FF3.6-15 */
			background: -webkit-linear-gradient(top, #eeeeee 0%,#cccccc 100%); /* Chrome10-25,Safari5.1-6 */
			background: linear-gradient(to bottom, #eeeeee 0%,#cccccc 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eeeeee', endColorstr='#cccccc',GradientType=0 ); /* IE6-9 */
			border-radius: 30px;
			color: black;
		}
		select{
			text-align: center !important;
			font-size: 12px !important;
			    border-radius: 20px;
			    border: 1px solid #999 !important;
			    padding: 0px !important;
			    /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#eeeeee+0,cccccc+100 */
				background: #eeeeee; /* Old browsers */
				background: -moz-linear-gradient(top, #eeeeee 0%, #cccccc 100%); /* FF3.6-15 */
				background: -webkit-linear-gradient(top, #eeeeee 0%,#cccccc 100%); /* Chrome10-25,Safari5.1-6 */
				background: linear-gradient(to bottom, #eeeeee 0%,#cccccc 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eeeeee', endColorstr='#cccccc',GradientType=0 ); /* IE6-9 */
			    color: black !important;
			    min-width: 50px !important;
		}
		.xfooter a{
			color: #222 !important;
			text-shadow: 0px 5px 30px rgba(0,0,0,0.3);
		}
		.xfooter{
			
			color: #222 !important;
			position: fixed;
			left: 0;
			bottom: 0;
			width: 100%;
			font-size: 12px;
			padding: 12px;
			background-color: rgba(0,0,0,0.1);
		}
		.btn:hover{
				/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#eeeeee+0,cccccc+100 */
				/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#499bea+0,207ce5+100;Blue+3d+%237 */
				background: #499bea; /* Old browsers */
				background: -moz-linear-gradient(top, #499bea 0%, #207ce5 100%) !important; /* FF3.6-15 */
				background: -webkit-linear-gradient(top, #499bea 0%,#207ce5 100%) !important; /* Chrome10-25,Safari5.1-6 */
				background: linear-gradient(to bottom, #499bea 0%,#207ce5 100%) !important; /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#499bea', endColorstr='#207ce5',GradientType=0 ) !important; /* IE6-9 */
				color:white !important;
				text-shadow: 0px 1px 1px rgba(0,0,0,0.5) !important;
				border-color: rgba(0,0,0,0.1);
			    min-width: 100px !important;
		}
		.btn span{
			display: block !important;
			padding: 1px !important;
			padding-left: 10px !important;
			padding-right: 10px !important;
			min-width: 100px !important;
			border-radius: 3px !important;
		}

		.btn[type=submit] {
			font-size: 12px !important;
			    border: 1px solid #999 !important;
				/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#eeeeee+0,cccccc+100 */
				/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#499bea+0,207ce5+100;Blue+3d+%237 */
				background: #499bea; /* Old browsers */
				background: -moz-linear-gradient(top, #499bea 0%, #207ce5 100%) !important; /* FF3.6-15 */
				background: -webkit-linear-gradient(top, #499bea 0%,#207ce5 100%) !important; /* Chrome10-25,Safari5.1-6 */
				background: linear-gradient(to bottom, #499bea 0%,#207ce5 100%) !important; /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#499bea', endColorstr='#207ce5',GradientType=0 ) !important; /* IE6-9 */
				color:white;
				text-shadow: 0px 1px 1px rgba(0,0,0,0.5) !important;
				border-color: rgba(0,0,0,0.1);
			    min-width: 100px !important;
			    padding: 2px;
		}
		input[type=search]{
			padding: 5px;
			margin: 0px;
			width: 100px !important;
		}
		
		.modal-content{
			animation-name : openwindow;
			animation-duration: 0.3s;
			overflow: hidden;
			box-shadow: 0px 20px 80px rgba(0,0,0,0.9);
			border: 1px solid silver;
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e0f3fa', endColorstr='#b6dffd',GradientType=0 ); /* IE6-9 */
			border-radius: 10px;
		}
		.modal-body{
			background-color: #e8e8e8;
			max-height: 800px;
			overflow: auto;
		}
		.dropdown-menu{
			border-radius: 4px;
			padding: 5px;
			box-shadow: 0px 2px 2px rgba(0,0,0,0.3);
			animation-name : opendropdown;
			animation-duration: 0.2s;
			font-size: 12px;
		}
		.modal-header{
			font-size: 13px !important;
			padding: 5px;
			border-bottom: 1px solid rgba(0,0,0,0.3);
			text-shadow: 0px 0px 10px white !important;
				background: #eeeeee; /* Old browsers */
				background: -moz-linear-gradient(top, #eeeeee 0%, #cccccc 100%); /* FF3.6-15 */
				background: -webkit-linear-gradient(top, #eeeeee 0%,#cccccc 100%); /* Chrome10-25,Safari5.1-6 */
				background: linear-gradient(to bottom, #eeeeee 0%,#cccccc 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eeeeee', endColorstr='#cccccc',GradientType=0 ); /* IE6-9 */

		}
		.modal-backdrop {
		background-color: transparent;
		}
		@keyframes openwindow{
			0%{

				transform: scale(0.5);
				filter: blur(2px);
			}
			100%{

			}
		}
		@keyframes opendropdown{
			0%{
				margin-top: -10px;
				filter: blur(5px);
			}
			100%{

			}
		}
			@keyframes blurredout{
			0%{
				transform: scale(0.9);
			}
			100%{

			}
		}
		a{
			color: #03A9F4;
		}
		.modal-footer{
			background-color: #e0e0e0;
		}
		table{
			border-color: rgba(0,0,0,0.3) !important;
		}
		tr{
			padding: 5px !important;
			font-size: 12px;
			border-color: rgba(0,0,0,0.3) !important;
		}
		td{
			padding: 5px !important;
			font-size: 12px;
			border-color: rgba(0,0,0,0.3) !important;
		}
		th{
			text-shadow: 0px 1px 0px rgba(255,255,255,0.7) !important;
			background-color: rgba(0,0,0,0.1);
			border-bottom: 1px !important;
			border-top: 1px !important;
			vertical-align: middle !important;
			padding: 2px !important;
			text-align: center;
			color: black !important;
			font-size: 12px;
			border-color: rgba(0,0,0,0.3) !important;
		}
		.card{
			border-radius: 10px;
			overflow: hidden;
			box-shadow: 0px 2px 5px rgba(0,0,0,0.4);
			margin-bottom: 20px;
		}
		.card-header{
			color: #707078 !important;
			font-size: 13px !important;
			padding: 10px;
			border-bottom: 1px solid rgba(0,0,0,0.3);
			text-shadow: 0px 0px 10px white !important;
				background: #eeeeee; /* Old browsers */
				background: -moz-linear-gradient(top, #eeeeee 0%, #cccccc 100%); /* FF3.6-15 */
				background: -webkit-linear-gradient(top, #eeeeee 0%,#cccccc 100%); /* Chrome10-25,Safari5.1-6 */
				background: linear-gradient(to bottom, #eeeeee 0%,#cccccc 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eeeeee', endColorstr='#cccccc',GradientType=0 ); /* IE6-9 */

		}
		.card-body{
			border-radius: 0px;
			padding: 5px;
			background-color: white;
		}
		.navbar{
			/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#eeeeee+0,cccccc+100;Gren+3D */
			background: #eeeeee; /* Old browsers */
			background: -moz-linear-gradient(top, #eeeeee 0%, #cccccc 100%); /* FF3.6-15 */
			background: -webkit-linear-gradient(top, #eeeeee 0%,#cccccc 100%); /* Chrome10-25,Safari5.1-6 */
			background: linear-gradient(to bottom, #eeeeee 0%,#cccccc 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eeeeee', endColorstr='#cccccc',GradientType=0 ); /* IE6-9 */
			padding: 5px;
			font-size: 12px !important;
			box-shadow: 0px 1px 30px rgba(0,0,0,0.4);
		}
		.navbar a{
			color: black !important;
			margin-left : 5px;
			margin-right : 5px;
			padding: 2px;
			padding-left: 10px;
			padding-right: 10px;
			padding-top: 0px;
			padding-bottom: 0px;
			text-shadow: 0px 1px 0px rgba(255,255,255,1) !important;
		}
		.modal-header .ptitle{
			width: 70%;
			text-shadow: 0px 1px 0px rgba(255,255,255,1) !important;
			font-size: 15px;
			text-align: center;
			width: 100%;
		}
		.modal-header .ptitle small{
			color: black;
		}
		.modal-header .ptitle img{
			float: left;
		}
		.labelinput{
			text-align: center; border: none; background-color: transparent !important;
		}

</style>
