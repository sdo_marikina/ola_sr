<style type="text/css">
		.form-control{
			padding-left: 15px;
			padding-right: 15px;
			padding-top: 2px;
			padding-bottom: 2px;
			font-size: 12px;
			height: 30px;
			border-bottom: 1px solid rgba(0,0,0,0.08);
			border-left: 1px solid rgba(0,0,0,0.08);
			border-radius: 2px;
		}
		.jumbotron{
				/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#1e5799+0,7db9e8+100 */
				background: #1e5799; /* Old browsers */
				background: -moz-linear-gradient(top, #1e5799 0%, #7db9e8 100%); /* FF3.6-15 */
				background: -webkit-linear-gradient(top, #1e5799 0%,#7db9e8 100%); /* Chrome10-25,Safari5.1-6 */
				background: linear-gradient(to bottom, #1e5799 0%,#7db9e8 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1e5799', endColorstr='#7db9e8',GradientType=0 ); /* IE6-9 */
				color: white;
		}
		.btn{
				font-size: 12px;
			    border-radius: 3px;
			    border: 1px solid #999;
			    padding: 0px;
			    background: #F0F0F0;
			    background: -moz-linear-gradient(top,  #F0F0F0 50%, #D4D4D4 50%);
			    background: -webkit-gradient(linear, left top, left bottom, color-stop(50%,#F0F0F0), color-stop(50%,#D4D4D4));
			    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#F0F0F0', endColorstr='#D4D4D4',GradientType=0 );
			    color: black !important;
		}
		select{
			font-size: 12px !important;
			    border-radius: 3px !important;
			    border: 1px solid #999 !important;
			    padding: 0px !important;
			    background: #F0F0F0 !important;
			    background: -moz-linear-gradient(top,  #F0F0F0 50%, #D4D4D4 50%) !important;
			    background: -webkit-gradient(linear, left top, left bottom, color-stop(50%,#F0F0F0), color-stop(50%,#D4D4D4)) !important;
			    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#F0F0F0', endColorstr='#D4D4D4',GradientType=0 ) !important;
			    color: black !important;
			    min-width: 50px !important;
		}
		.btn span{
			display: block !important;
			border: 1px solid white !important;
			padding: 1px !important;
			min-width: 100px !important;
			border-radius: 3px !important;
		}

		.btn[type=submit] {
			font-size: 12px !important;
			    border-radius: 3px !important;
			    border: 1px solid #999 !important;
			    padding-top: 2px !important;
			    padding-bottom: 2px !important;
			    background: #F0F0F0 !important;
			    background: -moz-linear-gradient(top,  #F0F0F0 50%, #D4D4D4 50%) !important;
			    background: -webkit-gradient(linear, left top, left bottom, color-stop(50%,#F0F0F0), color-stop(50%,#D4D4D4)) !important;
			    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#F0F0F0', endColorstr='#D4D4D4',GradientType=0 ) !important;
			    color: black !important;
			    min-width: 100px !important;
		}
		
		.modal-content{
			animation-name : openwindow;
			animation-duration: 0.3s;
			overflow: hidden;
			box-shadow: 0px 0px 20px rgba(0,0,0,1);
			padding: 5px;
			border: 1px solid white;



			/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#e0f3fa+0,d8f0fc+50,b8e2f6+51,b6dffd+100;Shape+2+Style */
			background: #e0f3fa; /* Old browsers */
			background: -moz-linear-gradient(top, #e0f3fa 0%, #d8f0fc 50%, #b8e2f6 51%, #b6dffd 100%); /* FF3.6-15 */
			background: -webkit-linear-gradient(top, #e0f3fa 0%,#d8f0fc 50%,#b8e2f6 51%,#b6dffd 100%); /* Chrome10-25,Safari5.1-6 */
			background: linear-gradient(to bottom, #e0f3fa 0%,#d8f0fc 50%,#b8e2f6 51%,#b6dffd 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e0f3fa', endColorstr='#b6dffd',GradientType=0 ); /* IE6-9 */

		}
		.modal-body{
			background-color: white;
			border-left: 1px solid rgba(0,0,0,0.3);
			border-right: 1px solid rgba(0,0,0,0.3);
			border-top: 1px solid rgba(0,0,0,0.3);
			max-height: 800px;
			overflow: auto;
		}
		.dropdown-menu{
			border-radius: 0px;
			padding: 5px;
			box-shadow: 0px 2px 2px rgba(0,0,0,0.3);
			animation-name : opendropdown;
			animation-duration: 0.2s;
			font-size: 12px;
		}
		.modal-header{
			font-size: 13px !important;
			padding: 5px;
			border-bottom: 1px rgba(0,0,0,0.3);
			text-shadow: 0px 0px 10px white !important;
		}
		.modal-backdrop {
		background-color: transparent;
		}
		@keyframes openwindow{
			0%{
				opacity: 0;
				transform: scale(0.9);
			}
			100%{

			}
		}
		@keyframes opendropdown{
			0%{
				opacity: 0;
			}
			100%{

			}
		}
		a{
			color: #03A9F4;
		}
		.modal-footer{
			border: 1px solid rgba(0,0,0,0.3);
			border-top: 1px solid rgba(0,0,0,0.05);
			background-color: #EEF2F8;
		}
		tr{
			padding: 5px !important;
			font-size: 12px;
		}
		td{
			padding: 5px !important;
			font-size: 12px;

		}
		th{
			vertical-align: middle !important;
			padding: 2px !important;
/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#f2f7f9+0,ffffff+100 */
background: #f2f7f9; /* Old browsers */
background: -moz-linear-gradient(top, #f2f7f9 0%, #ffffff 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top, #f2f7f9 0%,#ffffff 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, #f2f7f9 0%,#ffffff 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f2f7f9', endColorstr='#ffffff',GradientType=0 ); /* IE6-9 */
			text-align: center;
			color: #707078 !important;
			font-size: 12px;
		}
		.card{
			border-radius: 0px;
		}
		.card-header{
			color: #707078 !important;
			border-radius: 0px;
			padding: 5px;
			/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#f2f7f9+0,ffffff+100 */
background: #f2f7f9; /* Old browsers */
background: -moz-linear-gradient(top, #f2f7f9 0%, #ffffff 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top, #f2f7f9 0%,#ffffff 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, #f2f7f9 0%,#ffffff 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f2f7f9', endColorstr='#ffffff',GradientType=0 ); /* IE6-9 */
		}
		.card-body{
			border-radius: 0px;
			padding: 5px;

		}
		.navbar{
			/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#e5f0ff+0,e8f4f9+50,d7e8f7+52,d1e4ff+100 */
background: #e5f0ff; /* Old browsers */
background: -moz-linear-gradient(top, #e5f0ff 0%, #e8f4f9 50%, #d7e8f7 52%, #d1e4ff 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top, #e5f0ff 0%,#e8f4f9 50%,#d7e8f7 52%,#d1e4ff 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, #e5f0ff 0%,#e8f4f9 50%,#d7e8f7 52%,#d1e4ff 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e5f0ff', endColorstr='#d1e4ff',GradientType=0 ); /* IE6-9 */
padding: 5px;
font-size: 12px !important;
border-bottom: 1px solid rgba(255,255,255,0.3);
box-shadow: 0px 1px 1px rgba(0,0,0,0.4);
		}
		.navbar a{
			color: #243549 !important;
			margin-left : 5px;
			margin-right : 5px;
			padding: 2px;
			padding-left: 10px;
			padding-right: 10px;
			padding-top: 0px;
			padding-bottom: 0px;
		}
		.ptitle{
color: #3F51B5;
width: 70%;
}
		.ptitle small{
color: black;
}
.ptitle img{
	float: left;
	padding-right: 5px;
}
.labelinput{
	text-align: center; border: none; background-color: white !important;
}

</style>
