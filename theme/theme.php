<?php  $version = json_encode("3.2.1.15.3"); ?>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script type="text/javascript" src="qrpower/qrcode.js"></script>

	<script src="api/tracking.js-master/build/tracking-min.js"></script>
	<script src="api/tracking.js-master/build/data/face-min.js"></script>
	<script type="text/javascript" src="api/webcamjs-master/webcam.min.js"></script>
	
	<script src="api/chartjs/Chart.bundle.js"></script>
	<script src="api/chartjs/Chart.bundle.min.js"></script>
	<script src="api/chartjs/Chart.js"></script>
	<script src="api/chartjs/Chart.min.js"></script>

	<?php
	include("theme/essentials/essentials.php");
	include("theme/design_MacOS.php");
	?>

	<link rel="icon" href="theme/sdo.ico" type="text/css" href="">

<script type="text/javascript">
	
	$("html").append('<div class="notifbox">' +
'<h4 class="not_title">Notification</h4>' +
'<p class="not_desc">Hello, World!</p>' +
'</div>');
function popnotification(title, message,isrefresh){
$(".notifbox").css("display","block");
$(".not_title").html(title);
$(".not_desc").html(message);
$(".notifbox").css("margin-right","20px");
setTimeout(function(){
$(".notifbox").css("display","none");
if(isrefresh == true){
	window.location.reload();
}},5000)}

</script>

	<?php
	include("components/module_sidebar.php");
	?>
