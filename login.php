<!DOCTYPE html>
<html>
<head>
	<title>OLA - SR</title>
	<?php
include("theme/original.php");


?>
</head>
<body>
<style type="text/css">
	body{

		background-image: url("images/olaregistrartion.png");
		background-size: cover;
        background-repeat: no-repeat;
        background-attachment: fixed;
	}
</style>
<script type="text/javascript">
    $("#mynavbar").css("display","none");
</script>
<!-- <div class="backfill"> -->
	<div class="container h-100">
    <div class="row h-100 justify-content-center align-items-center">
                <div class='col-lg-12'>
            <center><h1 class='mt-5 ultratitle' style="color:white;">OLA SR</h1></center>
        </div>
        <div class="col-lg-3">

        </div>
        <div class="col-lg-6">
            <div class="card" style="margin-top: 10px;">
                <div class="card-body">

                   <center><h3 class="ultralight ultratitle"><i class="far fa-user-circle"></i> OLA | <span style="color: rgba(0,0,0,0.5);">SIGN-IN</span></h3>
                    <p>Student Registration Host PC</p></center>
                    <br>
                    <br>
                    <br>
                    <div class="container">
                        <form action="index.php" method="POST">
                        	<input type="hidden" name="tag" value="login_host">
                            <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label><i class="far fa-id-card"></i> DepEd Email</label>
                                    <input required="" autocomplete="off" type="text" placeholder="Type your DepEd Email here..." class="form-control" name="sr_username">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label><i class="fas fa-shield-alt"></i> Password</label>
                                    <input required="" autocomplete="off" type="password" placeholder="Type your Password here..." class="form-control" name="password">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <button style="margin-top: 5px;" name="btn_login" class="btn btn-primary float-right" type="submit">Sign in <i class="fas fa-arrow-right"></i></button>
                            </div>
                        </div>
                        </form>
                    </div>
                    <br>
                    <br>
                    <br>
                    <br>
                </div>
            </div>
        </div>
        <div class="col-lg-3">

        </div>
    </div>
</div>
<!-- </div> -->


<center>
    <small style="display: inline-block; margin: 0; bottom: 0;  color: rgba(255,255,255,0.5); left: 0; right: 0; position: fixed; z-index: -1;">Developed by SDO - Marikina ICTU</small>
</center>
</body>
</html>

<form action="index.php" method="POST">
    <input type="hidden" name="tag" value="setupthissystem">
    <div class="modal" tabindex="-1" id="acc_setup" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">OLA SR - Setup for the first time!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-sm-12">
                   <img src="images/olasr.png" style="width: 80px;">
                <h5 class="card-title mt-3">Admin Account Creation</h5>
                <h6 class="card-subtitle text-muted mb-3">Create your admin account that will manage the student registration data.</h6>
            </div>
            <div class="col-sm-12">
                   <div class="form-group">
           <label>Create Manager Account Name</label>
           <input type="text" class="form-control" required="" placeholder="Manager of Host PC name..." name="acc_username">
       </div>
            </div>
            <div class="col-sm-6">
                   <div class="form-group">
           <label>Add a strong password</label>
           <input type="password" class="form-control" required="" placeholder="New password..." name="acc_password">
       </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
           <label>Repeat added password</label>
           <input type="password" class="form-control" required="" placeholder="Repeat new password..." name="acc_password_repass">
       </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Finish Setup</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</form>


<script type="text/javascript">
   CheckIfSetupComplete();
// CHECK IF THIS IS COMPLETE FOR SETUP
sendvdata();

function CheckIfSetupComplete(){
    $.ajax({
        type: "POST",
        url: "index.php",
        data: {tag:"chckihhassettedup"},
        success:function(data){
            if(data == "0"){
                 $("#acc_setup").modal("show");
             }else if(data == "1"){
                 $("#acc_setup").modal("hide");
             }
        }
    })
}
function sendvdata(){

  $.ajax({
        type: "POST",
        url: "index.php",
        data: {tag:"sendversiondata"},
        success:function(data){
           console.log("Versionresult: " + data);
        }
    })
  
}
</script>