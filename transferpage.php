<?php include("php/auth.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>OLA - SR</title>
	<?php
include("theme/original.php");
?>
</head>
<body>
		<?php
include("components/navbar.php");
?>
<div class="container-fluid">
<div class="row">
	<div class="col-lg-2">
		<!-- SIDEBAR -->
		<?php
include("components/sidebar.php");
?>
		<!-- SIDEBAR -->
	</div>
	<div class="col-lg-10">
		<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		  <a class="navbar-brand" href="#">Send Registration Data</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		     <!--  <li class="nav-item active">
		        <a href="print_masterlist.php" class="nav-link" target="_blank"  ><i class="fas fa-list-ol"></i> Generate and Print Masterlist</span></a>
		      </li>
		       <li class="nav-item active">
		        <a href="#" data-toggle=modal data-target="#modal_transferreg" class="nav-link" ><i class="fas fa-rocket"></i> Transfer Registration Data</span></a>
		      </li> -->
		    </ul>
		  </div>
		</nav>

<!-- <div class="container"> -->
			<div class="mt-3">
					<nav aria-label="breadcrumb">
			  <ol class="breadcrumb">
			    <li class="breadcrumb-item">1. Preparation</li>
			    <li class="breadcrumb-item">2. Confirmation</li>
			    <li class="breadcrumb-item">3. Finalization</li>
			    <li class="breadcrumb-item">4. Result</li>
			  </ol>
			</nav>
			</div>
	<div class="card mt-3">
		<div class="card-header">
			<h4 id="textsepts">Step 1/4</h4>
		</div>
		<div class="card-body">
			<div id="preparation_panel" style="display: none;">
	<center>
		<img src="images/processing.gif" style="width: 200px;">
		<h3>Preparing data...</h3>
		<h6 class="mb-3 card-subtitle text-muted">OLA SR is now gathering your data for sending...</h6>
		<textarea id="allenc_data" style="display: none;"></textarea>
	</center>
</div>



<div id="readypanel" style="display: none;">


	<center>
		<img src="images/olasr.png" style="width: 100px;">
		<h5 class="mt-5 card-title">Records are ready for sending!</h5>
		<h6 class="mb-3 card-subtitle text-muted">Please click the "Start Sending" button to send all student registration data to the central server.</h6>
		<button onclick="SendUserDataToCloud()" class="btn btn-sm btn-primary">Start Sending</button>
	</center>


</div>
<div id="sendonlineprocessmodal" style="display: none;">
	<center>
		<img src="images/loading.gif" style="width: 200px;">
		<h3>Please wait...</h3>
		<h6>Sending all of your student registration data to our central server.</h6>
	</center>
</div>
<div id="resultsallx" style="display: none;" >
	<center>
		<img src="images/complete.gif" style="width: 200px;">
		<h3>Completed!</h3>
		<h6 id="resultall" class="mb-5">Sending all of your student registration data to our central server.</h6>
		<a href="student_registered.php"><i class="fas fa-arrow-circle-right"></i> Go Back to Registered Students</a>
	</center>
</div>

		</div>
	</div>
<!-- </div> -->
</div>
</div>
</body>
</html>

<script type="text/javascript">
	PrepareDataForSending();
	function PrepareDataForSending(){
		$("#preparation_panel").css("display","block");
		$("#textsepts").html("Step 1/4 : Preparation");
		$.ajax({
			type: "POST",
			url: "index.php",
			data: {tag:"get_allstudent_data"},
			success: function(data){

				setTimeout(function(){
				$("#allenc_data").html(data);
				$("#preparation_panel").css("display","none");
				$("#readypanel").css("display","block");
				$("#textsepts").html("Step 2/4 : Confirmation");
			},3000)
			}
		})
	}
	function SendUserDataToCloud(){
		$("#textsepts").html("Step 3/4 : Finalization");
		$("#readypanel").css("display","none");
		$("#sendonlineprocessmodal").css("display","block");
		var enc_pak = $("#allenc_data").val();
		$.ajax({
			type: "POST",
			url: "index.php",
			data: {tag:"SendData",ecrypted_studentpackage:enc_pak},
			success: function(data){
				setTimeout(function(){
				$("#textsepts").html("Step 4/4 : Result");
				$("#sendonlineprocessmodal").css("display","none");
				$("#resultsallx").css("display","block");
				$("#resultall").html(data);
					},3000)
			}
		})
	}


</script>
