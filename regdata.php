<?php include("php/auth.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>OLA - SR</title>
	<?php
include("theme/original.php");
?>
</head>
<body>
		<?php
include("components/navbar.php");
?>
<div class="container-fluid">
<div class="row">
	<div class="col-lg-2">
		<!-- SIDEBAR -->
		<?php
include("components/sidebar.php");
?>
		<!-- SIDEBAR -->
	</div>
	<div class="col-lg-10">
				<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		  <a class="navbar-brand" href="#">Configurations</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		       <li class="nav-item active">
		        <a href="#" data-toggle="modal" data-target="#modal_reseteve" class="nav-link" ><i class="fas fa-sync"></i> Reset Everything</span></a>
		      </li>

		      <li class="nav-item active">
		        <a href="#" data-toggle="modal" data-target="#modal_wipeout" class="nav-link" ><i class="fas fa-trash-alt"></i> Clear Registration Data</span></a>
		      </li>
<li class="nav-item active">
		        <a href="#" data-toggle="modal" data-target="#myconlink" class="nav-link"><i class="fas fa-arrows-alt-h"></i> Get Webservice Link</a>
		      </li>
		    </ul>
		  </div>
		</nav>

<div class="modal" tabindex="-1" role="dialog" id="myconlink">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Connection Link</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<p>Put this connection link to your workstations</p>
        <h3><?php echo "http://" . getHostByName(getHostName()) . "/ola_sr/index.php"; ?></h3>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<form action="index.php" method="POST">
			<div class="modal" tabindex="-1" role="dialog" id="modal_wipeout">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Clear All Registration Data?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<input type="hidden" name="tag" value="wipeoutdatastudents">
        <p>This will wipe out every encoded student data in your system.</p>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-danger"><i class="far fa-trash-alt"></i> The data that is about to clear is a sample data.</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
</form>
		<form action="index.php" method="POST">
		
			<div class="modal" tabindex="-1" role="dialog" id="modal_reseteve">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Reset Everything</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<h3 class="mb-3">THIS WILL REQUIRE FOR SETTING-UP THE ENTIRE SYSTEM AGAIN.</h3>
      	<input type="hidden" name="tag" value="reset_everything">
        <p class="mb-5">This will delete your current Student Registration data, System Preferences and head you back to the first-time setup.</p>
        <small class="text-muted"><i>If you can't see the reset button now and wonder why, the reset button will only be available after 20 seconds after the page loads.</i></small>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-danger" id="cocoa" style="display: none;"><i class="fas fa-sync"></i> Continue Reset</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
		</form>

		<div class="row">
			<div class="col-sm-12">
		<div class="mt-3 container">
			
				<div id="id_prefcounts">
					
				</div>
		
		</div>
			</div>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">

	setTimeout(function(){
$("#cocoa").css("display","block");
	},20000)
	LoadPrefConts();
	function LoadPrefConts(){
		$.ajax({
			type: "POST",
			url: "index.php",
			data: {tag: "get_prefs"},
			success: function(data){
				$("#id_prefcounts").html(data);
			}
		})
	}
</script>
<form action="index.php" method="POST">
	<input type="hidden" name="tag" value="change_pref">
	<div class="modal" tabindex="-1" id="modal_makechanges" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="mk_ch">Make Changes</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<input type="hidden" name="pref_id" id="pref_id">
        <div class="form-group">
        	<label>Value</label>
        	<input type="text" required="" class="form-control" name="pref_val" id="id_ofvalprefs">
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</form>
<script type="text/javascript">
	function open_prefchange(control_obj){
		$("#pref_id").val($(control_obj).data("pid"));
		$("#id_ofvalprefs").val($(control_obj).data("pref_curr_val"));
		$("#mk_ch").html($(control_obj).data("pref_name"));
	}
</script>
