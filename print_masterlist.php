<!DOCTYPE html>
<html>
<head>
	<title>Print Registration Info</title>
</head>
<body>
<?php
include("theme/original.php");
$c = mysqli_connect("localhost","root","","ola_sr");
$q = "SELECT * FROM registered_students LEFT JOIN station_info ON registered_students.school = station_info.station_id ";
$res = mysqli_query($c,$q);
$row = mysqli_fetch_all($res,MYSQLI_ASSOC);

if(mysqli_num_rows($res) == 0){
?>
<div class="container">
	<h1 class="mt-5">There's nothing to generate.</h1>
	<h6>Hello! you currently have no registered students yet.</h6>
	<p class="mt-5">-OLA SR</p>
</div>
<?php

}else{
	?>

	<?php


$q = "SELECT * FROM preferences WHERE pref_name='sn' LIMIT 1";
$res = mysqli_query($c,$q);
$row = mysqli_fetch_array($res);
$mystationname = $row["value"];


?>

<div class="container">
	<h5 class="card-title mt-3"><strong><?php echo $mystationname; ?></strong></h5>
	<h6 class="card-subtitle text-muted">Registred Student masterlist</h6>
	<table class="mt-3 table table-bordered table-sm">
		<thead>
			<tr>
				<th>No#</th>
				<th>Name</th>
			</tr>
		</thead>
		<tbody>
			<?php

			$q = "SELECT * FROM registered_students";
			$res = mysqli_query($c,$q);
			$count = 0;
			while ($row = mysqli_fetch_array($res)) {
				$count++;
				echo "
					<tr>
						<td>" . $count . "</td>
						<td>" . $row["last_name"] . ", " . $row["first_name"] . " " . $row["middle_name"] . $row["ext_name"] . "</td>
					</tr>
				";
			}

			?>
		</tbody>
	</table>
</div>
<?php
}
?>
</body>
</html>