<!DOCTYPE html>
<html>
<head>
	<title>OLA - SR</title>
	<?php
include("theme/original.php");


?>
</head>
<body>
<style type="text/css">
	body{

		background-image: url("images/olaregistrartion.png");
		background-size: cover;
        background-repeat: no-repeat;
        background-attachment: fixed;
	}
</style>
<div class="container" style="color:white;">


<h5 class="card-title mt-5">System Activation</h5>
<h6 class="card-subtitle mb-4">Enter the required connection key for the first time to start using OLA SR!</h6>
<div class="row">
  <div class="col-sm-6">
    <div class="form-group">
  <label>Client Primary Key</label>
  <input type="text" class="form-control" placeholder="type here..." id="inp_p1" name="">
</div>
  </div>
  <div class="col-sm-6">
    <div class="form-group">
  <label>Client Secondary Key</label>
  <input type="text" class="form-control" placeholder="type here..." id="inp_p2" name="">
</div>
  </div>
</div>

<div class="form-group">
  <button class="btn btn-light mt-3 btn-sm"><i class="far fa-times-circle"></i></button>
  <button onclick="VerifySystem()" class="btn btn-light mt-3 btn-sm"><i class="fas fa-arrow-circle-right"></i> Connect</button>
</div>



</div>
</body>
</html>
<script type="text/javascript">
  function VerifySystem(){
    var pp1 = $("#inp_p1").val();
    var pp2 = $("#inp_p2").val();
    if(pp1 != "" && pp2 != ""){
      $.ajax({
      type: "POST",
      url: "index.php",
      data: {tag:"sys_verifiy_now",p1: pp1,p2: pp2},
      success: function(data){
       if(data == "error"){
        $("#inp_p1").val("");
        $("#inp_p2").val("");
         alert("Invalid Key!");
       }else if(data = "success"){
        alert("Your system is successfully activated!");
        window.location.href = "host.php";
       }
      }
    })
    }else{
      alert("Please complete all the verification key fields!");
    }
  }
</script>