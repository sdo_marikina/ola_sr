<?php
$c = mysqli_connect("localhost","root","","ola_sr");
mysqli_set_charset($c,"utf8");
date_default_timezone_set("Asia/Manila");
$tag = $_POST["tag"];
$passkey = "virmil";
switch ($tag) {
	case 'save_edit_student':
	AddToColumn("registered_students","is_sped","VARCHAR(222) DEFAULT '0'");
	// GET MY STATION ID
	$q = "SELECT * FROM preferences WHERE pref_name='snum'";
	$res = mysqli_query($c,$q);
	$row = mysqli_fetch_array($res);



	//ORIGIN APPLICATION INFO
		$station_id = $row["value"];
	// STUDENT INFO
		$lrn = $_POST["lrn"];
		$studentgradelevel = $_POST["studentgradelevel"];
		$lastname = strtoupper( mysqli_real_escape_string($c,$_POST["lastname"]));
		$firstname = strtoupper( mysqli_real_escape_string($c,$_POST["firstname"]));
		$middlename = strtoupper( mysqli_real_escape_string($c,$_POST["middlename"]));
		$extname = strtoupper( mysqli_real_escape_string($c,$_POST["extname"]));
		$gender = $_POST["gender"];
		$age = $_POST["age"];
		$birthdate = $_POST["birthdate"];
		$birthplace = strtoupper( mysqli_real_escape_string($c,$_POST["birthplace"]));

		$previous_school = strtoupper( mysqli_real_escape_string($c,$_POST["previous_school"]));
		$school_year = $_POST["school_year"];
		$grade_level_previous = $_POST["grade_level_previous"];

		$street = strtoupper(mysqli_real_escape_string($c,$_POST["street"]));
		$barangay =strtoupper( mysqli_real_escape_string($c,$_POST["barangay"]));
		$municipality = strtoupper(mysqli_real_escape_string($c,$_POST["municipality"]));

		$fathersname = strtoupper(mysqli_real_escape_string($c,$_POST["fathersname"]));
		$mothersname = strtoupper(mysqli_real_escape_string($c,$_POST["mothersname"]));
		$contact_number_parents = $_POST["contact_number_parents"];

		$guardiansname = strtoupper(mysqli_real_escape_string($c,$_POST["guardiansname"]));
		$guardiansrelationship = strtoupper(mysqli_real_escape_string($c,$_POST["guardiansrelationship"]));
		$contact_number_guardians = $_POST["contact_number_guardians"];
		$is_sped = "0";

		if(isset($_POST["is_sped"])){
			$is_sped  = strtolower(mysqli_real_escape_string($c,$_POST["is_sped"]));
		}
		
		$q = "UPDATE registered_students SET
		syattended='" . $school_year . "',
		prevschool='" . $previous_school . "',
		lrn='" . $lrn . "',
		level='" . $studentgradelevel . "',
		prev_level='" . $grade_level_previous . "',
		picture='',
		school='" . $station_id . "',
		last_name='" . $lastname . "',
		first_name='" . $firstname . "',
		middle_name='" . $middlename  . "',
		ext_name='" . $extname . "',
		sex='" . $gender . "',
		age='" . $age . "',
		birthday='" . $birthdate  . "',
		birth_place='" . $birthplace . "',
		address='" . $street . "',
		barangay='" . $barangay . "',
		city='" . $municipality . "',
		mothers_name='" . $mothersname . "',
		mcontact='" . $contact_number_parents . "',
		fathers_name='" . $fathersname . "',
		fcontact='" . $contact_number_parents . "',
		guardians_name='" . $guardiansname . "',
		guardians_relationship='" . $guardiansrelationship . "',
		guardians_contact='" . $contact_number_guardians . "',
		is_sped='" . $is_sped . "',
		section='none',
		synched='0'
		 WHERE enrl_code='" . $_POST["reference_code"] . "'";

		$res = mysqli_query($c,$q) or die(mysqli_error($c));
		if($res){
			echo "true";
		}else{
			echo "false";
		}
SendVersionData();
		break;
	case 'request_data_info':
	$q = "SELECT * FROM registered_students WHERE id='" . $_POST["data_id"] . "' LIMIT 1";
	$res = mysqli_query($c,$q);
	$row = mysqli_fetch_array($res);
	$row["birthday"] = date("Y-m-d",strtotime($row["birthday"]));
	echo json_encode($row);
		break;
	case 'sendversiondata':
		echo sendversiondata();
		break;
	case 'wipeoutdatastudents':
	$q = "DELETE FROM registered_students";
	$res = mysqli_query($c,$q);
	goback("regdata","All cleared!");
		break;
	case 'getallsentinfocount':
	$sentdata = 0;
	$overall = 0;
		$q = "SELECT * FROM registered_students";
		$res = mysqli_query($c,$q);
		$overall = number_format( mysqli_num_rows($res));

		$q = "SELECT * FROM registered_students WHERE synched='1'";
		$res = mysqli_query($c,$q);
		$sentdata = number_format(mysqli_num_rows($res));

		echo $sentdata . "/" . $overall;
		break;
	case 'print_reg_tocsv':
header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename="Registered Students from OLA SR.csv"');
$data = array("Reference Number, Previous School Year, Previous School, Picture, LRN,Grade Level, School Number, Last Name, First Name, Middle Name, Extension Name, Sex, Birthday,Age, Birthplace, Address,Barangay, City/Municipality, Mother, Contact, Father, Contact,Guradian, Relationship, Contact, Date Registered, Section, Previous Grade Level, Encoded By,Sped");
$q = "SELECT * FROM registered_students";
$res = mysqli_query($c,$q);
while($row = mysqli_fetch_array($res)){
	$gender = "Male";
	$is_sped = "No";
	if($row["sex"] == "0"){
		$gender = "Female";
	}
	if($row["is_sped"] == "1"){
		$is_sped = "Yes";
	}
	$toadd = 
	$row["enrl_code"] . ','
	. $row["syattended"] . ','
	. $row["prevschool"] . ','
	. $row["picture"] . ','
	. $row["lrn"] . ','
	. $row["level"] . ','
	. $row["school"] . ','
	. $row["last_name"] . ','
	. $row["first_name"] . ','
	. $row["middle_name"] . ','
	. $row["ext_name"] . ','
	. $gender . ','
	. $row["birthday"] . ','
	. $row["age"] . ','
	. $row["birth_place"] . ','
	. $row["address"] . ','
	. $row["barangay"] . ','
	. $row["city"] . ','
	. $row["mothers_name"] . ','
	. $row["mcontact"] . ','
	. $row["fathers_name"] . ','
	. $row["fcontact"] . ','
	. $row["guardians_name"] . ','
	. $row["guardians_relationship"] . ','
	. $row["guardians_contact"] . ','
	. $row["date_register"] . ','
	. $row["section"] . ','
	. $row["prev_level"] . ','
	. $row["origin_encoder"] . ","
	. $is_sped;
	array_push($data, $toadd);
}
$fp = fopen('php://output', 'wb');
foreach ( $data as $line ) {
    $val = explode(",", $line);
    fputcsv($fp, $val);
}
fclose($fp);

		break;
case 'getifhasregisteredstudentnow':
	$q = "SELECT * FROM registered_students";
	if(mysqli_num_rows(mysqli_query($c,$q)) ==0){
		echo "false";
	}else{
		echo "true";
	}
	break;
	case 'calculate_age':
		  //date in mm/dd/yyyy format; or it can be in other formats as well
  $birthDate = "12/17/1983";
  //explode the date to get month, day and year
  $birthDate = explode("/", $birthDate);
  //get age from date or birthdate
  $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
    ? ((date("Y") - $birthDate[2]) - 1)
    : (date("Y") - $birthDate[2]));
  echo "Age is:" . $age;
		break;
	case 'fromapp_getstationinfo':
	$station_info = "";
	$q = "SELECT * FROM preferences WHERE pref_name='snum' LIMIT 1";
	$res = mysqli_query($c,$q);
	$row = mysqli_fetch_array($res);
	$station_info .= $row["value"];

	$q = "SELECT * FROM preferences WHERE pref_name='sn' LIMIT 1";
	$res = mysqli_query($c,$q);
	$row = mysqli_fetch_array($res);
	$station_info .= "|" . $row["value"];

	echo $station_info;
	break;
	case 'SendData':
// SENING OF DATA
	$q = "SELECT * FROM preferences WHERE pref_name='wser' LIMIT 1";
	$res = mysqli_query($c,$q);
	$row = mysqli_fetch_array($res);
	$serverlink = $row["value"];

	$tagkey = sdm_encrypt("TransferStudentData",$passkey);
	$package = $_POST["ecrypted_studentpackage"];

	$form_data = array(
	'tag'=>$tagkey,
	'enc_student_data'=>$package ,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$serverlink);
	// curl_setopt($ch, CURLOPT_URL,"http://localhost:8080/ola_srm_webservice/index.php");
	curl_setopt($ch,CURLOPT_POST,1);
	// curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
	$output = sdm_decrypt($output,$passkey);
	echo $output;
	$q = "UPDATE registered_students SET synched='1'";
	$res = mysqli_query($c,$q);
		break;
	case 'smartsuggest':
	$searchkeyword = strtoupper(mysqli_real_escape_string($c,htmlentities($_POST["searchkeyword"])));
	$stype = $_POST["searchtype"];
		$q = "SELECT * FROM registered_students WHERE " . $stype . " LIKE '%" . $searchkeyword . "%' GROUP BY " . $stype . " LIMIT 4";
		$res = mysqli_query($c,$q);
		while ($row = mysqli_fetch_array($res)) {
			echo $row[$stype] . "|";
		}
echo "ss";
		break;
	case 'checkifstationisactivated':
	$q = "SELECT * FROM preferences WHERE pref_name='verif' LIMIT 1";
		$res = mysqli_query($c,$q);
	$row = mysqli_fetch_array($res);
	echo $row["value"];
		break;
	case "sys_verifiy_now":
	$q = "SELECT * FROM preferences WHERE pref_name='wser' LIMIT 1";
	$res = mysqli_query($c,$q);
	$row = mysqli_fetch_array($res);
	$serverlink = $row["value"];

	$tagkey = sdm_encrypt("sys_verifiy_now",$passkey);
	$prkey1 = sdm_encrypt($_POST["p1"],$passkey);
	$prkey2 = sdm_encrypt($_POST["p2"],$passkey);

	$form_data = array(
	'tag'=>$tagkey,
	'p1'=>$prkey1,
	'p2'=>$prkey2,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$serverlink);
	curl_setopt($ch,CURLOPT_POST,1);
	// curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
	$output = sdm_decrypt($output,$passkey);

	if($output != "false" && count($output) != 0){
		$output = json_decode($output,true);
		if(count($output) != 0){
			$q = "UPDATE preferences SET value='1' WHERE pref_name='verif'";
		$res = mysqli_query($c,$q);
		if($res){
			//CLEAR STATION INFO IF HAS
			$q = "DELETE FROM station_info";
			$res = mysqli_query($c,$q);
			// SETUP STATION INFO
			$q = "INSERT INTO station_info SET station_id='" . $output[0]["station_number"] . "',full_station_name='" . $output[0]["station_name"] . "'";
			$res = mysqli_query($c,$q);
			// SETUP STATION NAME
			$q = "UPDATE preferences SET value='" . $output[0]["station_name"] ."' WHERE pref_name='sn'";
		$res = mysqli_query($c,$q);
		//SETUP STATION 
		$q = "UPDATE preferences SET value='" . $output[0]["station_number"] ."' WHERE pref_name='snum'";
$res = mysqli_query($c,$q);
		//SETUP PRIMARY 
		$q = "UPDATE preferences SET value='" . $output[0]["station_code"] ."' WHERE pref_name='sl'";
$res = mysqli_query($c,$q);
		//SETUP SECONDARY 
		$q = "UPDATE preferences SET value='" . $output[0]["station_secret_code"] ."' WHERE pref_name='csk'";
		$res = mysqli_query($c,$q);
			//SUCCESS VERIFICATION
			echo "success";
		}else{
			//FAILED STATION INFO INSERSATION
			echo "error";
		}
	}else{
		// VERIFICATION OR DECODE FAILED
		echo "error";
	}
	}else{
		// NO PRIMARY AND SECONDARY KEY FOUND
		echo "error";
	}
	
	break;
	case "checkifexisitingaccount":
	$acc_name = $_POST["acc_name"];
	$acc_pass = sdm_encrypt($_POST["acc_pass"],$passkey);
	$q = "SELECT * FROM users WHERE username='" . $acc_name ."' AND password='" . $acc_pass ."' LIMIT 1";

	$res = mysqli_query($c,$q);
	
	if(mysqli_num_rows($res) == 0){
		echo "false";
	}else{
		echo "true";
	}
	break;
	case 'connect_check':
		echo "connection_found";
		break;
	case 'chckihhassettedup':
		$q = "SELECT * FROM admin";
		echo json_encode(mysqli_num_rows(mysqli_query($c,$q)));
	break;
	case 'setupthissystem':
	if ($_POST["acc_password"] == $_POST["acc_password_repass"]) {
			$username = mysqli_real_escape_string($c,htmlentities($_POST["acc_username"]));
			$password = sdm_encrypt($_POST["acc_password"],$passkey);

			$q = "INSERT INTO admin SET username='" . $username  . "', password='" . $password  . "', station='n/a'";
			$res = mysqli_query($c,$q);

			if($res){
					header("location: login.php");
			}else{
					header("location: login.php");
			}
		}else{
		header("location: login.php");
	}
			break;
			case 'reset_everything':

			$q = "UPDATE preferences SET value='0' WHERE pref_name='verif'";
			$res = mysqli_query($c,$q);


			if(session_status() == PHP_SESSION_NONE){
			session_start();
			}
			session_unset();
			session_destroy();
			header("location: login.php");
	

		break;
	case 'searchitlikeaman':
	if(mysqli_real_escape_string($c,htmlentities($_POST["searchkey"])) == ""){
echo "";
	}else{
		$keyword = strtoupper(mysqli_real_escape_string($c,htmlentities($_POST["searchkey"])));
		$q = "SELECT * FROM registered_students  WHERE
			enrl_code LIKE '%" . $keyword . "%' OR 
			prevschool LIKE '%" . $keyword . "%' OR 
			lrn LIKE '%" . $keyword . "%' OR 
			level LIKE '%" . $keyword . "%' OR 
			prev_level LIKE '%" . $keyword . "%' OR 
			school LIKE '%" . $keyword . "%' OR 
			last_name LIKE '%" . $keyword . "%' OR 
			first_name LIKE '%" . $keyword . "%' OR 
			middle_name LIKE '%" . $keyword . "%' OR 
			birthday LIKE '%" . $keyword . "%' OR 
			birth_place LIKE '%" . $keyword . "%' OR 
			address LIKE '%" . $keyword . "%' OR 
			barangay LIKE '%" . $keyword . "%' OR 
			city LIKE '%" . $keyword . "%' OR 
			mothers_name LIKE '%" . $keyword . "%' OR 
			fathers_name LIKE '%" . $keyword . "%' OR 
			guardians_name LIKE '%" . $keyword . "%' OR 
			guardians_relationship LIKE '%" . $keyword . "%' LIMIT 6";
			$res = mysqli_query($c,$q);
			if(mysqli_num_rows($res) == 0){
				echo "There's no matching result. Try again";
			}else{
				while ($row= mysqli_fetch_array($res)) {
				echo "
				<div class='mt-2 scalable'>
<button class='btn btn-sm btn-primary float-right' data-toggle='modal' data-target='#edittheinfo' onclick='openedit(this)' data-eid='" . $row["id"] . "'><i class='far fa-edit'></i> Edit</button>
				<a target='_blank' title='Click to see all info about this search result.' href='print_form.php?my_reg_id=" . $row["id"] . "'>
				<h6><i class='fas fa-search'></i> " . $row["last_name"] . ", " . $row["first_name"] . " " . $row["middle_name"] . " " . $row["ext_name"] . "</h6></a>
				<small class='text-muted'><strong>Age: </strong>" . $row["age"] . "y <strong>Level: </strong>" . $row["level"] . " <strong>Ref: </strong>" . $row["enrl_code"] . "<br> <strong>LRN: </strong>" . $row["lrn"];

				if($row["sex"] == "1"){
					echo " <strong>Sex: </strong> Male";
				}else{
						echo " <strong>Sex: </strong> Female";
				}
				echo " <strong>Birthday: </strong> " . date("F d, Y",strtotime($row["birthday"]));
				echo "</small>
				
				</div>";
			}
			}
			
	}
		
		break;
	case 'get_allstudent_data':
		$q = "SELECT * FROM registered_students WHERE synched='0'";
		$res = mysqli_query($c,$q);
		$row = mysqli_fetch_all($res,MYSQLI_ASSOC);
		echo sdm_encrypt(json_encode($row),$passkey);
	break;
	case "get_reg_enc":
		$q = "SELECT * FROM users";
		$res = mysqli_query($c,$q);
		echo number_format(mysqli_num_rows($res));
	break;
	case 'get_all_regstu_info_recent':
	$q = "SELECT * FROM registered_students ORDER BY id DESC LIMIT 64 ";
		$res = mysqli_query($c,$q);
		$count = 0;
		while ($row = mysqli_fetch_array($res)) {
			$count++;


			$is_sped = "SPED Registered";

			if( $row["is_sped"] == "0" ){
				$is_sped = "N/A";
			}
			echo "
				<tr>
				<td>" . $count . "</td>
				<td>" . $row["lrn"] . "</td>

				<td><a target='_blank' href='print_form.php?my_reg_id=" . $row["id"] . "'>" . $row["last_name"] . ", " . $row["first_name"] . " " . $row["middle_name"] . " " . $row["ext_name"] . "</a></td>
				<td>";

				if($row["sex"] == "0"){
					echo "Female";
				}else{
					echo "Male";
				}
				echo "</td>
				<td>" . $row["age"] . "</td>
				<td>" . date("F d, Y",strtotime($row["birthday"])). "</td>
				<td>" . $row["barangay"] . "</td>
				<td>" . $is_sped . "</td>
				<td>" . date("F d, Y",strtotime($row["date_register"])) . "</td>
				<td><button data-toggle='modal' data-target='#edittheinfo' onclick='openedit(this)' data-eid='" . $row["id"] . "' class='btn btn-sm btn-primary'><i class='fas fa-edit'></i> Edit</button></td>
				</tr>
			";
		}
		break;
	case "get_all_regstu_info":
		$q = "SELECT * FROM registered_students";
		$res = mysqli_query($c,$q);
		$count = 0;
		while ($row = mysqli_fetch_array($res)) {
			$count++;
			$is_sped = "SPED Registered";

			if( $row["is_sped"] == "0" ){
				$is_sped = "N/A";
			}
			echo "
				<tr>
				<td>" . $count . "</td>
				<td>" . $row["lrn"] . "</td>
		
				<td><a target='_blank' href='print_form.php?my_reg_id=" . $row["id"] . "'>" . $row["last_name"] . ", " . $row["first_name"] . " " . $row["middle_name"] . " " . $row["ext_name"] . "</a></td>
				<td>";

				if($row["sex"] == "0"){
					echo "Female";
				}else{
					echo "Male";
				}
				echo "</td>
				<td>" . $row["age"] . "</td>
				<td>" . date("F d, Y",strtotime($row["birthday"])). "</td>
				<td>" . $row["barangay"] . "</td>
					<td>" . $is_sped . "</td>
				<td>" . date("F d, Y",strtotime($row["date_register"])) . "</td>
				<td><button data-toggle='modal' data-target='#edittheinfo' onclick='openedit(this)' data-eid='" . $row["id"] . "' class='btn btn-sm btn-primary'><i class='fas fa-edit'></i> Edit</button></td>
				</tr>
			";
		}
	break;
	case 'change_pref':
		$q = "UPDATE preferences SET value='" . $_POST["pref_val"] . "' WHERE id='" . $_POST["pref_id"] . "'";
		$res = mysqli_query($c,$q);
		header("location: regdata.php");
		break;
	case "get_prefs":
	$q = "SELECT * FROM preferences WHERE hidden='0'";
	$res = mysqli_query($c,$q);
	while ($row = mysqli_fetch_array($res)) {
		echo "
		<div class='card mb-3'><div class='card-body'>
		<small>
			<a href='#' data-toggle='modal' data-target='#modal_makechanges' onclick='open_prefchange(this)' data-pref_name='" .  $row["name"]. "' data-pref_curr_val='" .  $row["value"]. "' data-pid='" . $row["id"] . "' class='float-right'><i class='fas fa-pencil-alt'></i> Edit</a>
		</small>
		<h6 class='text-muted'>" . $row["name"] . "</h6><p>";

		if($row["value"]  == ""){
			echo "(empty)";
		}else{
			echo $row["value"] ;
		}
		echo "</p></div></div>";
	}
	break;
	case "update_encoder":
		$q = "UPDATE users SET
		username='" . $_POST["xname"] . "',
		date_updated='" . date("Y-m-d H:i:s") ."'
		WHERE 
		id='" . $_POST["xid"] . "'
		";
		$res = mysqli_query($c,$q);
		header("location: encoders.php");
	break;
	case "delete_encoder":
$q = "DELETE FROM users
		WHERE 
		id='" . $_POST["xid"] . "'
		";
		$res = mysqli_query($c,$q);
		header("location: encoders.php");
	break;
	case "get_registered_encoders":
	$q = "SELECT * FROM users";
	$res = mysqli_query($c,$q);
	while($row = mysqli_fetch_array($res)){
		echo "
			<tr>
				<td>" . htmlentities($row["username"]) . "</td>
				<td>" . date("F d, Y g:i a",strtotime($row["date_added"])) . "</td>
				<td>" . date("F d, Y g:i a",strtotime($row["date_updated"])) . "</td>
				<td>
					<div class='dropdown'>
  <a class='btn btn-primary btn-sm dropdown-toggle' href='#' role='button' id='dropdownMenuLink' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
    Action
  </a>

  <div class='dropdown-menu' aria-labelledby='dropdownMenuLink'>
    <a class='dropdown-item' href='#' onclick='open_updateencoder(this)' data-uname='" . $row["username"] . "' data-oid='" . $row["id"] . "' data-toggle='modal' data-target='#modal_up'>Edit</a>
    <a class='dropdown-item' href='#' onclick='open_deleteencoder(this)' data-oid='" . $row["id"] . "' data-toggle='modal' data-target='#modal_del'>Delete</a>
  </div>
</div>
				</td>
			</tr>
		";
	}
	break;
	case "add_new_encoder":

		$uname =strtolower($_POST["uname"]);
		$pass = $_POST["pass"];
		$repass = $_POST["repass"];

		$q = "SELECT * FROM users WHERE username='" . $uname . "'";

		if(mysqli_num_rows(mysqli_query($c,$q)) == 0){
			if($pass == $repass){
			$q = "INSERT INTO users SET 
		username='" . $uname . "',
		password='" . sdm_encrypt($pass,$passkey) . "',
		date_added='" . date("Y-m-d H:i:s") . "',
		date_updated='" . date("Y-m-d H:i:s") . "'";

		$res = mysqli_query($c,$q) or die(mysqli_error($c));

			header("location: encoders.php");
		}else{
			header("location: encoders.php");
		}
	}else{
		header("location: encoders.php");
	}	
	break;
	case 'logout':
	if(session_status() == PHP_SESSION_NONE){
			session_start();
			}
	session_unset();
	session_destroy();
	header("location: login.php");
		break;
	case "login_host":
	AddToColumn("registered_students","is_sped","VARCHAR(222) DEFAULT '0'");
		$q = "SELECT * FROM admin WHERE username='" . $_POST["sr_username"] . "' AND password='" . sdm_encrypt($_POST["password"],$passkey) . "'";
		$res = mysqli_query($c,$q);
		if(mysqli_num_rows($res) != 0){
			if(session_status() == PHP_SESSION_NONE){
			session_start();
			}
			$_SESSION["sr_username"] =  $_POST["sr_username"];

			//CHECK VERIFICATION
			$q = "SELECT * FROM preferences WHERE pref_name='verif' LIMIT 1";
			$res = mysqli_query($c,$q);
			$row = mysqli_fetch_array($res);
			// echo $row["value"];
			if($row["value"] == "0"){
				header("location: verification_screen.php");
			}else if($row["value"] == "1"){
				header("location: host.php");
			}
			
		}else{
			header("location: login.php");
		}
	break;
	case 'countofallreg':
		$q = "SELECT id FROM registered_students";
		echo number_format(mysqli_num_rows(mysqli_query($c,$q)));
		break;
		// SPED 

	case "getsumbyage_sped":
		$lvl_order = array("Kindergarten","Grade 1");
			$q = "SELECT * FROM registered_students WHERE is_sped='1' GROUP BY age ORDER BY age ASC";
			$res = mysqli_query($c,$q);
			$row = mysqli_fetch_all($res,MYSQLI_ASSOC);

			for($i = 0; $i < count($row);$i++){
				echo "<tr>
				<td>" . number_format($row[$i]["age"]) . "</td>";
				for($y = 0 ; $y < count($lvl_order);$y++){
				// HORIZONTAL GENERATION BY LEVEL
				$lvl = $lvl_order[$y];

				$w = "SELECT *,count(case when sex = 0 then 1 end) as female, count(case when sex = 1 then 1 end) as male FROM registered_students WHERE age='" . $row[$i]["age"]  . "' AND level='" . $lvl . "' AND is_sped='1'";




				$w_res = mysqli_query($c,$w);
				$w_row = mysqli_fetch_all($w_res,MYSQLI_ASSOC);
				for($o = 0; $o < count($w_row);$o++){


				$mm = $w_row[$o]["male"];
				$ff = $w_row[$o]["female"];
				$mper = "0";
				$fper = "0";
				$kin_total = $mm + $ff;
				//CALCULATE PERCENTAGE
				if(	$kin_total  > 0){
				$mper = number_format(($mm / $kin_total) * 100) . "%";
				$fper = number_format(($ff / $kin_total) * 100) . "%";
				}



					echo "
				<td>" . number_format($mm) . "</td>
				<td>" . $mper . "</td>
				<td>" . number_format($ff) . "</td>
				<td>" . $fper ."</td>
				<td>" . number_format($kin_total) . "</td>
				";
				}
				
				}
				echo "</tr>";
			}
	break;
	case "getsumbycount_sped":
		$kin_m_count = 0;
		$kin_f_count = 0;
		$kin_total = 0;

		$kin_m_per = 0;
		$kin_f_per =0;


		// GET MALES
		$q = "SELECT * FROM registered_students WHERE sex='1' AND level='Kindergarten' AND is_sped='1'";
		$res = mysqli_query($c,$q);
		$kin_m_count = mysqli_num_rows($res);
		// GET FEMALES
		$q = "SELECT * FROM registered_students WHERE sex='0' AND level='Kindergarten' AND is_sped='1'";
		$res = mysqli_query($c,$q);
		$kin_f_count = mysqli_num_rows($res);


		$kin_total = $kin_m_count + $kin_f_count;
		//CALCULATE PERCENTAGE
			if(	$kin_total  > 0){
			$kin_m_per = number_format(($kin_m_count / $kin_total) * 100) . "%";
		$kin_f_per = number_format(($kin_f_count / $kin_total) * 100) . "%";
		}

		$toecho = "
			<tr>
			<td>" . number_format($kin_m_count) . "</td>
			<td>" . $kin_m_per . "</td>
			<td>" . number_format($kin_f_count) . "</td>
			<td>" . $kin_f_per . "</td>
			<td>" . number_format($kin_total) . "</td>
		";


		$kin_m_count = 0;
		$kin_f_count = 0;
		$kin_total = 0;

		$kin_m_per = 0;
		$kin_f_per =0;


		// GET MALES
		$q = "SELECT * FROM registered_students WHERE sex='1' AND level='Grade 1' AND is_sped='1'";
		$res = mysqli_query($c,$q);
		$kin_m_count = mysqli_num_rows($res);
		// GET FEMALES
		$q = "SELECT * FROM registered_students WHERE sex='0' AND level='Grade 1' AND is_sped='1'";
		$res = mysqli_query($c,$q);
		$kin_f_count = mysqli_num_rows($res);


		$kin_total = $kin_m_count + $kin_f_count;
		//CALCULATE PERCENTAGE
		if(	$kin_total  > 0){
			$kin_m_per = number_format(($kin_m_count / $kin_total) * 100) . "%";
		$kin_f_per = number_format(($kin_f_count / $kin_total) * 100) . "%";
		}
		

		$toecho .= "
			
			<td>" . number_format($kin_m_count) . "</td>
			<td>" . $kin_m_per . "</td>
			<td>" . number_format($kin_f_count) . "</td>
			<td>" . $kin_f_per . "</td>
			<td>" . number_format($kin_total) . "</td>
			</tr>
		";
		echo $toecho;
	break;
		// SPED
	case "getsumbyage":
		$lvl_order = array("Kindergarten","Grade 1");
			$q = "SELECT * FROM registered_students WHERE is_sped='0' GROUP BY age ORDER BY age ASC";
			$res = mysqli_query($c,$q);
			$row = mysqli_fetch_all($res,MYSQLI_ASSOC);

			for($i = 0; $i < count($row);$i++){
				echo "<tr>
				<td>" . number_format($row[$i]["age"]) . "</td>";
				for($y = 0 ; $y < count($lvl_order);$y++){
				// HORIZONTAL GENERATION BY LEVEL
				$lvl = $lvl_order[$y];

				$w = "SELECT *,count(case when sex = 0 then 1 end) as female, count(case when sex = 1 then 1 end) as male FROM registered_students WHERE age='" . $row[$i]["age"]  . "' AND level='" . $lvl . "' AND is_sped='0'";




				$w_res = mysqli_query($c,$w);
				$w_row = mysqli_fetch_all($w_res,MYSQLI_ASSOC);
				for($o = 0; $o < count($w_row);$o++){


				$mm = $w_row[$o]["male"];
				$ff = $w_row[$o]["female"];
				$mper = "0";
				$fper = "0";
				$kin_total = $mm + $ff;
				//CALCULATE PERCENTAGE
				if(	$kin_total  > 0){
				$mper = number_format(($mm / $kin_total) * 100) . "%";
				$fper = number_format(($ff / $kin_total) * 100) . "%";
				}



					echo "
				<td>" . number_format($mm) . "</td>
				<td>" . $mper . "</td>
				<td>" . number_format($ff) . "</td>
				<td>" . $fper ."</td>
				<td>" . number_format($kin_total) . "</td>
				";
				}
				
				}
				echo "</tr>";
			}
	break;
	case "getsumbycount":
		$kin_m_count = 0;
		$kin_f_count = 0;
		$kin_total = 0;

		$kin_m_per = 0;
		$kin_f_per =0;


		// GET MALES
		$q = "SELECT * FROM registered_students WHERE sex='1' AND level='Kindergarten' AND is_sped='0'";
		$res = mysqli_query($c,$q);
		$kin_m_count = mysqli_num_rows($res);
		// GET FEMALES
		$q = "SELECT * FROM registered_students WHERE sex='0' AND level='Kindergarten' AND is_sped='0'";
		$res = mysqli_query($c,$q);
		$kin_f_count = mysqli_num_rows($res);


		$kin_total = $kin_m_count + $kin_f_count;
		//CALCULATE PERCENTAGE
			if(	$kin_total  > 0){
			$kin_m_per = number_format(($kin_m_count / $kin_total) * 100) . "%";
		$kin_f_per = number_format(($kin_f_count / $kin_total) * 100) . "%";
		}

		$toecho = "
			<tr>
			<td>" . number_format($kin_m_count) . "</td>
			<td>" . $kin_m_per . "</td>
			<td>" . number_format($kin_f_count) . "</td>
			<td>" . $kin_f_per . "</td>
			<td>" . number_format($kin_total) . "</td>
		";


		$kin_m_count = 0;
		$kin_f_count = 0;
		$kin_total = 0;

		$kin_m_per = 0;
		$kin_f_per =0;


		// GET MALES
		$q = "SELECT * FROM registered_students WHERE sex='1' AND level='Grade 1' AND is_sped='0'";
		$res = mysqli_query($c,$q);
		$kin_m_count = mysqli_num_rows($res);
		// GET FEMALES
		$q = "SELECT * FROM registered_students WHERE sex='0' AND level='Grade 1' AND is_sped='0'";
		$res = mysqli_query($c,$q);
		$kin_f_count = mysqli_num_rows($res);


		$kin_total = $kin_m_count + $kin_f_count;
		//CALCULATE PERCENTAGE
		if(	$kin_total  > 0){
			$kin_m_per = number_format(($kin_m_count / $kin_total) * 100) . "%";
		$kin_f_per = number_format(($kin_f_count / $kin_total) * 100) . "%";
		}
		

		$toecho .= "
			
			<td>" . number_format($kin_m_count) . "</td>
			<td>" . $kin_m_per . "</td>
			<td>" . number_format($kin_f_count) . "</td>
			<td>" . $kin_f_per . "</td>
			<td>" . number_format($kin_total) . "</td>
			</tr>
		";
		echo $toecho;
	break;
	case "get_student_registration_id":

		$q = "SELECT * FROM registered_students WHERE
		first_name='" . strtoupper($_POST["first_name"]) . "' AND
		middle_name='" . strtoupper($_POST["middle_name"]) . "' AND
		last_name='" . strtoupper($_POST["last_name"]) . "' AND
		age='" . $_POST["age"] . "' AND
		sex='" . $_POST["sex"] . "' AND
		birth_place='" . strtoupper($_POST["birth_place"]) . "' AND
		address='" . strtoupper($_POST["street"]) . "'

		LIMIT 1";

		$res = mysqli_query($c,$q) or die(mysqli_error($c));
		$row = mysqli_fetch_all($res,MYSQLI_ASSOC);
		if(count($row) == 1){
			//MERON
			echo $row[0]["id"];
		}else{
			// WALA
			echo "false";
		}
	break;
	case 'getallregisteredcount':
		$q = "SELECT id FROM registered_students";
		echo mysqli_num_rows(mysqli_query($c,$q));
	break;
	case 'search_student_info':
		$keyword = strtoupper($_POST["keyword"]);
		$q = "SELECT * FROM registered_students WHERE
			enrl_code LIKE '%" . $keyword . "%' OR 
			prevschool LIKE '%" . $keyword . "%' OR 
			lrn LIKE '%" . $keyword . "%' OR 
			level LIKE '%" . $keyword . "%' OR 
			prev_level LIKE '%" . $keyword . "%' OR 
			school LIKE '%" . $keyword . "%' OR 
			last_name LIKE '%" . $keyword . "%' OR 
			first_name LIKE '%" . $keyword . "%' OR 
			middle_name LIKE '%" . $keyword . "%' OR 
			birthday LIKE '%" . $keyword . "%' OR 
			birth_place LIKE '%" . $keyword . "%' OR 
			address LIKE '%" . $keyword . "%' OR 
			barangay LIKE '%" . $keyword . "%' OR 
			city LIKE '%" . $keyword . "%' OR 
			mothers_name LIKE '%" . $keyword . "%' OR 
			fathers_name LIKE '%" . $keyword . "%' OR 
			guardians_name LIKE '%" . $keyword . "%' OR 
			guardians_relationship LIKE '%" . $keyword . "%' LIMIT 32";

			$res = mysqli_query($c,$q) or die(mysqli_error($c));

			if($res){
				if(mysqli_num_rows($res) != 0){
					//HAS RESULT
					while ($row = mysqli_fetch_array($res)) {
						$toecho =  $row["lrn"] . "|" . $row["last_name"] . ", " .  $row["first_name"] . " " . $row["middle_name"] . " " . $row["ext_name"]   . "|" . $row["enrl_code"] . "|" . $row["id"] . "~";
						echo strtoupper($toecho);
					}
				}else{
					//NO RESULT
					echo "No Result";
				}
			}
		break;
	case 'setup_new_host':
		$station_id = $_POST["station_id"];
		$station_fullname = $_POST["station_fullname"];
		// CHECK IF HAS ACCOUNT ALREADY
		$q = "SELECT * FROM station_info";
		if(mysqli_num_rows(mysqli_query($c,$q)) ==0){
			//ADD NEW
		$q = "INSERT INTO station_info SET station_id='" . $station_id . "',full_station_name='" . $station_fullname . "'";
		if(mysqli_query($c,$q)){
			//SUCCESS
		echo "success";
		}else{
			//ERROR
			echo "error";
		}
		}else{
			// EXISTING
		echo "existing";
		}
	break;
	case 'checkifhasinfo':
		$q = "SELECT * FROM station_info LIMIT 1";
		$res = mysqli_query($c,$q);
		if(mysqli_num_rows($res) != 0){
			$row = mysqli_fetch_array($res);
			echo $row["station_id"] . "," . $row["full_station_name"];
		}else{
			echo "false";
		}
	break;
	case 'submit_student_data':
	AddToColumn("registered_students","is_sped","VARCHAR(222) DEFAULT '0'");
	// GET MY STATION ID
	$q = "SELECT * FROM preferences WHERE pref_name='snum'";
	$res = mysqli_query($c,$q);
	$row = mysqli_fetch_array($res);



	//ORIGIN APPLICATION INFO
		$station_id = $row["value"];
	// STUDENT INFO
		$lrn = $_POST["lrn"];
		$studentgradelevel = $_POST["studentgradelevel"];
		$lastname = strtoupper( mysqli_real_escape_string($c,$_POST["lastname"]));
		$firstname = strtoupper( mysqli_real_escape_string($c,$_POST["firstname"]));
		$middlename = strtoupper( mysqli_real_escape_string($c,$_POST["middlename"]));
		$extname = strtoupper( mysqli_real_escape_string($c,$_POST["extname"]));
		$gender = $_POST["gender"];
		$age = $_POST["age"];
		$birthdate = $_POST["birthdate"];
		$birthplace = strtoupper( mysqli_real_escape_string($c,$_POST["birthplace"]));

		$previous_school = strtoupper( mysqli_real_escape_string($c,$_POST["previous_school"]));
		$school_year = $_POST["school_year"];
		$grade_level_previous = $_POST["grade_level_previous"];

		$street = strtoupper(mysqli_real_escape_string($c,$_POST["street"]));
		$barangay =strtoupper( mysqli_real_escape_string($c,$_POST["barangay"]));
		$municipality = strtoupper(mysqli_real_escape_string($c,$_POST["municipality"]));

		$fathersname = strtoupper(mysqli_real_escape_string($c,$_POST["fathersname"]));
		$mothersname = strtoupper(mysqli_real_escape_string($c,$_POST["mothersname"]));
		$contact_number_parents = $_POST["contact_number_parents"];

		$guardiansname = strtoupper(mysqli_real_escape_string($c,$_POST["guardiansname"]));
		$guardiansrelationship = strtoupper(mysqli_real_escape_string($c,$_POST["guardiansrelationship"]));
		$contact_number_guardians = $_POST["contact_number_guardians"];
		$orinenc = strtolower(mysqli_real_escape_string($c,$_POST["originencoder"]));
		$is_sped = "0";

		if(isset($_POST["is_sped"])){
			$is_sped  = strtolower(mysqli_real_escape_string($c,$_POST["is_sped"]));
		}
		$enrollment_code = "";
		if($lrn == "" || $lrn == "0" || $lrn == 0 || $lrn == null){
			$enrollment_code = $station_id . strtoupper(random_strings(13));
		}else{
			$enrollment_code = $station_id . substr($lrn,5,12) . strtoupper(random_strings(6));
		}
		
		$q = "INSERT INTO registered_students SET
		enrl_code='" . $enrollment_code . "',
		syattended='" . $school_year . "',
		prevschool='" . $previous_school . "',
		lrn='" . $lrn . "',
		level='" . $studentgradelevel . "',
		prev_level='" . $grade_level_previous . "',
		picture='',
		school='" . $station_id . "',
		last_name='" . $lastname . "',
		first_name='" . $firstname . "',
		middle_name='" . $middlename  . "',
		ext_name='" . $extname . "',
		sex='" . $gender . "',
		age='" . $age . "',
		birthday='" . $birthdate  . "',
		birth_place='" . $birthplace . "',
		address='" . $street . "',
		barangay='" . $barangay . "',
		city='" . $municipality . "',
		mothers_name='" . $mothersname . "',
		mcontact='" . $contact_number_parents . "',
		fathers_name='" . $fathersname . "',
		fcontact='" . $contact_number_parents . "',
		guardians_name='" . $guardiansname . "',
		guardians_relationship='" . $guardiansrelationship . "',
		guardians_contact='" . $contact_number_guardians . "',
		date_register='" . date("Y-m-d") . "',
		origin_encoder='" . $orinenc . "',
		is_sped='" . $is_sped . "',
		section='none'";

		$res = mysqli_query($c,$q) or die(mysqli_error($c));
		if($res){
			echo "true";
		}else{
			echo "false";
		}
SendVersionData();
		break;
	
	default:
	header("location:login.php");
		break;
}

function AddToColumn($tbl_name, $col_name, $col_datatype){
	global $c;
	$q = "SELECT $col_name FROM $tbl_name";
	$res =  mysqli_query($c,$q);
	if(!$res){
		//Column is not exsiting
		$q = "ALTER TABLE $tbl_name ADD $col_name $col_datatype";
		$res =  mysqli_query($c,$q);
	}
}

	function goback($pagename,$message){
		echo "
			<script>alert('" . $message . "');
			window.location.href = '" . $pagename . "';
			</script>
		";
	}

     function sdm_encrypt($data,$hash){
    $keycode = openssl_digest(utf8_encode($hash),"sha512",true);
    $string = substr($keycode, 10,24);
    $utfData = utf8_encode($data);
    $encryptData = openssl_encrypt($utfData, "DES-EDE3", $string, OPENSSL_RAW_DATA,'');
    $base64Data = base64_encode($encryptData);
    return $base64Data;
    }

     function sdm_decrypt($data,$hash){
        $keycode = openssl_digest(utf8_encode($hash),"sha512",true);
        $string = substr($keycode, 10,24);

        $utfData = base64_decode($data);
        $decryptData = openssl_decrypt($utfData, "DES-EDE3", $string, OPENSSL_RAW_DATA,'');
        return $decryptData;
    }

function random_strings($length_of_string) 
{ 
  
    // String of all alphanumeric character 
    $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 
  
    // Shufle the $str_result and returns substring 
    // of specified length 
    return substr(str_shuffle($str_result),  
                       0, $length_of_string); 
} 

function SendVersionData(){
global $c;
	global $passkey;
// CHECK IF SPED IS SENT IN FIRST TIME 
	$q = "SELECT * FROM preferences WHERE pref_name='hasfixederror'";
	if(mysqli_num_rows(mysqli_query($c,$q)) == 0){
		$q = "INSERT INTO preferences SET name='dump1',value='dump1',pref_name='hasfixederror',hidden='1'";
		mysqli_query($c,$q);
		$q = "UPDATE registered_students SET synched='0' WHERE is_sped='1'";
		mysqli_query($c,$q);
	}


	
	$q = "SELECT * FROM preferences WHERE pref_name='verif' LIMIT 1";
	$res = mysqli_query($c,$q);
	$xrow = mysqli_fetch_array($res);

	if($xrow["value"] == "1"){
	// SEND DATA IF VERIFIED


		$reglearners = "";

		$encoderscount = "";

		$version = "0.4";

		$station_name = "";

		// GET ENCODER COUNT 
$q = "SELECT id FROM  users";
	$res = mysqli_query($c,$q);
	$encoderscount = mysqli_num_rows($res);
		

		// GET REGISTERED LEARNERS COUNT
$q = "SELECT id FROM  registered_students";
	$res = mysqli_query($c,$q);
	$reglearners = mysqli_num_rows($res);

		// GET STATION NAME 
		$q = "SELECT * FROM preferences WHERE pref_name='sn' LIMIT 1";
	$res = mysqli_query($c,$q);
	$row = mysqli_fetch_array($res);
	$station_name = $row["value"];


// GET WEBSERVICE LINK 
$q = "SELECT * FROM preferences WHERE pref_name='wser' LIMIT 1";
	$res = mysqli_query($c,$q);
	$row = mysqli_fetch_array($res);
	$serverlink = $row["value"];

	$tagkey = sdm_encrypt("version_data_recieve",$passkey);


	$reglearners = sdm_encrypt($reglearners,$passkey);
	$encoderscount = sdm_encrypt($encoderscount,$passkey) ;
	$version = sdm_encrypt($version,$passkey);
	$station_name = sdm_encrypt($station_name,$passkey);
	$form_data = array(
	'tag'=>$tagkey,
	'sys_learners'=>$reglearners ,
	'sys_encoder'=>$encoderscount ,
	'sys_version'=>$version ,
	'sys_name'=>$station_name ,
	);

	$str = http_build_query($form_data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$serverlink);
	// curl_setopt($ch, CURLOPT_URL,"http://localhost:8080/ola_srm_webservice/index.php");
	curl_setopt($ch,CURLOPT_POST,1);
	// curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$output = curl_exec($ch);
	curl_close($ch);
	$output = sdm_decrypt($output,$passkey);
	return $output;
	}
}
?>