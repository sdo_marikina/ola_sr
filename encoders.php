<?php include("php/auth.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>OLA - SR</title>
	<?php
include("theme/original.php");
?>
</head>
<body>
		<?php
include("components/navbar.php");
?>
<div class="container-fluid">
<div class="row">
	<div class="col-lg-2">
		<!-- SIDEBAR -->
		<?php
include("components/sidebar.php");
?>
		<!-- SIDEBAR -->
	</div>
	<div class="col-lg-10">
				<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		  <a class="navbar-brand" href="#"> Encoder Accounts</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
		        <a target="_blank" class="nav-link" href="http://depedmarikina.ph" data-toggle="modal" data-target="#new_reg_modal"><i class="far fa-user-circle"></i> Register New Encoder</span></a>
		      </li>
		    </ul>
		  </div>
		</nav>
		

		<table class="table table-sm table-bordered table-striped mt-3">
			<thead>
				<tr>
					<th>Name</th>
					<th>Date Added</th>
					<th>Date Updated</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="tbl_myregencoders">
				
			</tbody>
		</table>

	</div>
</div>
</div>
</body>
</html>

<form action="index.php" method="POST">
	<div class="modal" tabindex="-1" id="modal_del" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title">Delete Encoder</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<input type="hidden" name="tag" value="delete_encoder">
	      	<input type="hidden" id="id_todelencoder" value="" name="xid">
	        <p>Are you sure you want delete this encoder?</p>
	      </div>
	      <div class="modal-footer">
	        <button type="submit" data-dismiss="modaL" class="btn btn-primary">Yes</button>
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>
</form>
<form action="index.php" method="POST">
	<div class="modal" tabindex="-1" id="modal_up" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title">Update Encoder</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<input type="hidden" name="tag" value="update_encoder">
	       <input type="hidden" id="id_toupdateencoder" name="xid">
	       	<div class="form-group">
	       	<label>DepEd Email</label>
	       	<input type="email" id="edit_fullname" class="form-control" name="xname">
	       </div>
	      </div>
	      <div class="modal-footer">
	        <button type="submit" data-dismiss="modaL" class="btn btn-primary">Save changes</button>
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>
</form>
<script type="text/javascript">

	function open_updateencoder(control_obj){
		$("#id_toupdateencoder").val($(control_obj).data("oid"));
		$("#edit_fullname").val($(control_obj).data("uname"));
	}

	function open_deleteencoder(control_obj){
		$("#id_todelencoder").val($(control_obj).data("oid"));
	}

	get_registered();
	function get_registered(){
		$.ajax({
			type: "POST",
			url: "index.php",
			data: {tag: "get_registered_encoders"},
			success: function(data){
				$("#tbl_myregencoders").html(data);
			}
		})
	}
</script>

<form action="index.php" method="POST">
	<input type="hidden" name="tag" value="add_new_encoder">
	<div class="modal" tabindex="-1" id="new_reg_modal" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title">Register New Encoder</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<input type="hidden" name="">
	       <div class="form-group">
	        	<label>DepEd Email</label>
	        	<input class="form-control" required="" autocomplete="off" type="email" name="uname">
	        </div>
	        <div class="row">
	        	<div class="col-sm-6">
	        		  <div class="form-group">
	        	<label>Password</label>
	        	<input class="form-control" required="" autocomplete="off" type="password" name="pass">
	        </div>
	        	</div>
	        	<div class="col-sm-6">
	        		<div class="form-group">
	        	<label>Re-type Password</label>
	        	<input class="form-control" type="password" required="" autocomplete="off" name="repass">
	        </div>
	        	</div>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="submit" class="btn btn-primary">Register as Encoder</button>
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>
</form>