	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <a class="navbar-brand" href="host.php">SDOMAR - OLA SR</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	
	  <div class="collapse navbar-collapse" id="navbarSupportedContent">
	  	<!-- <form class="form-inline">
    <input class="form-control mr-sm-2" type="search" placeholder="QUICK SIGHT" aria-label="Search">
  </form> -->
	    <ul class="navbar-nav mr-auto">
	     <!--  <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-dharmachakra"></i> SYSTEM
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="about.php"><i class="fas fa-robot"></i> About the System</a>
        </div>
      </li> -->
	    </ul>
	    <div class="dropdown">
	      <a class="btn btn-light dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="far fa-user-circle"></i> <?php echo strtoupper($_SESSION["sr_username"]); ?>
	      </a>
	    <form action="index.php" method="POST" class="form-inline my-2 my-lg-0">
	    	<input type="hidden" name="tag" value="logout">
	      <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
	          <button class="dropdown-item" type="submit">Sign-out</button>
	      </div>
	    </div>
	      </form>
	  
	  </div>
	</nav>