-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 25, 2020 at 05:06 PM
-- Server version: 5.7.21
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ola_sr`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(222) NOT NULL,
  `password` varchar(222) NOT NULL,
  `station` varchar(222) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `preferences`
--

DROP TABLE IF EXISTS `preferences`;
CREATE TABLE IF NOT EXISTS `preferences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `value` varchar(500) NOT NULL,
  `pref_name` varchar(222) NOT NULL,
  `hidden` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `preferences`
--

INSERT INTO `preferences` (`id`, `name`, `value`, `pref_name`, `hidden`) VALUES
(1, 'Webservice', 'http://ers.depedmarikina.ph/ola_srm_webservice/index.php', 'wser', 1),
(3, 'Station Name', '', 'sn', 1),
(4, 'Client Primary Key', '', 'sl', 0),
(5, 'Client Secondary Key', '', 'csk', 0),
(6, 'Station Number', '', 'snum', 1),
(7, 'Verification', '0', 'verif', 1);

-- --------------------------------------------------------

--
-- Table structure for table `registered_students`
--

DROP TABLE IF EXISTS `registered_students`;
CREATE TABLE IF NOT EXISTS `registered_students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enrl_code` varchar(50) NOT NULL,
  `syattended` varchar(255) NOT NULL,
  `prevschool` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `lrn` varchar(20) NOT NULL,
  `level` varchar(255) NOT NULL,
  `school` varchar(255) CHARACTER SET utf8 NOT NULL,
  `last_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `ext_name` varchar(255) NOT NULL,
  `sex` varchar(255) NOT NULL,
  `age` int(2) NOT NULL,
  `birthday` varchar(255) NOT NULL,
  `birth_place` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `barangay` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `mothers_name` varchar(255) NOT NULL,
  `mcontact` varchar(255) NOT NULL,
  `fathers_name` varchar(255) NOT NULL,
  `fcontact` varchar(255) NOT NULL,
  `guardians_name` varchar(255) NOT NULL,
  `guardians_relationship` varchar(255) NOT NULL,
  `guardians_contact` varchar(255) NOT NULL,
  `date_register` date NOT NULL,
  `section` varchar(222) NOT NULL,
  `prev_level` varchar(222) NOT NULL,
  `origin_encoder` varchar(222) NOT NULL,
  `synched` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `station_info`
--

DROP TABLE IF EXISTS `station_info`;
CREATE TABLE IF NOT EXISTS `station_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `station_id` varchar(222) NOT NULL,
  `full_station_name` varchar(222) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(222) NOT NULL,
  `password` varchar(222) NOT NULL,
  `date_added` timestamp NOT NULL,
  `date_updated` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
