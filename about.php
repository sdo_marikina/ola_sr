<?php include("php/auth.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>OLA - SR</title>
	<?php
include("theme/original.php");
?>
</head>
<body>
		<?php
include("components/navbar.php");
?>
<div class="container-fluid">
<div class="row">
	<div class="col-lg-2">
		<!-- SIDEBAR -->
		<?php
include("components/sidebar.php");
?>
		<!-- SIDEBAR -->
	</div>
	<div class="col-lg-10" >
		<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		  <a class="navbar-brand" href="#">About the System</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
		        <a target="_blank" class="nav-link" href="http://depedmarikina.ph"><i class="fas fa-globe"></i> Deped Website</span></a>
		      </li>
		    </ul>
		  </div>
		</nav>
		<div class="card mt-3">
			<div class="card-body">
				<img src="images/olasr.png" style="width: 100px;">
				<h5 class="card-title mt-3">OLA - Student Registration System <small>v.0.4</small></h5>
				<h6 class="card-subtitle text-muted">Developed by SDO - Marikina Information and Communication Technology Unit (ICTU)</h6>
			</div>
		</div>
	</div>
</div>
</div>
</body>
</html>