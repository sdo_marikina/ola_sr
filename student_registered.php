<?php include("php/auth.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>OLA - SR</title>
	<?php
include("theme/original.php");
?>
</head>
<body>
		<?php
include("components/navbar.php");
?>
<div class="container-fluid">
<div class="row">
	<div class="col-lg-2">
		<!-- SIDEBAR -->
		<?php
include("components/sidebar.php");
?>
		<!-- SIDEBAR -->
	</div>
	<div class="col-lg-10">
		<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		  <a class="navbar-brand" href="#">Registered Learners</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
		        <a href="print_masterlist.php" class="nav-link" target="_blank"  ><i class="fas fa-list-ol"></i> Generate and Print Masterlist</span></a>
		      </li>
		       <li class="nav-item active">
		        <a href="#" data-toggle=modal data-target="#modal_transferreg" class="nav-link" ><i class="fas fa-rocket"></i> Send Registration Data</span></a>
		      </li>
		      
		      <li class="nav-item active">
		        <a href="#" data-toggle=modal data-target="#modal_csvstudents" class="nav-link" ><i class="fas fa-file-csv"></i> Export to Excel</span></a>
		      </li>
		    </ul>
		  </div>
		</nav>


<div class="mt-3">
	<div class="row">
		<div class="col-sm-6">
			<div class="card">
				<div class="card-body">
					<img src="images/searching.gif" id="searchindicator" class="float-right" style="display:none; width: 50px;">
					<div class="row">
						<div class="col-sm-8">
							<div class="form-group">

						<label><i class="fas fa-search"></i> Search registration data</label>
				<input onchange='easysearch()' id="keywordinp" placeholder="Type student name, age, lrn, etc..." type="text" class="form-control form-control-lg" name="">
							</div>
						</div>
						<div class="col-sm-4">

						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<h6 class="text-muted mb-2"><i class="fas fa-rocket"></i> Registration Sending Info</h6>
			<h5><strong id="sentcountsumm">0/0</strong> registration data has been sent to SDO-Marikina</h5>
			<div class="alert alert-success" style="display: none;" id="pan_yes" role="alert">
			  <i class="fas fa-check-circle"></i> You have sent all of your data.
			</div>
			<div class="alert alert-warning" style="display: none;" id="pan_no" role="alert">
			  <i class="fas fa-exclamation-circle"></i> Data is waiting to be sent.
			</div>
		</div>
	</div>


<div class="modal" tabindex="-1" id="edittheinfo" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Manual Edit</h5>
        <input type="hidden" id="e_refcode" name="">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	 <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
      	  <li class="nav-item">
      	    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true"><i class="fas fa-user-circle"></i> Personal</a>
      	  </li>
      	  <li class="nav-item">
      	    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false"><i class="fas fa-phone-square"></i> Contact</a>
      	  </li>
      	  <li class="nav-item">
      	    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false"><i class="fas fa-redo-alt"></i> Previous School</a>
      	  </li>
      	</ul>
      	<div class="tab-content" id="pills-tabContent">
      	  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
      	  	<!-- PERSONAL INFORMATION -->
      	  		       <div class="row">
       	<div class="col-sm-3">
       		<div class="form-group">
       			<label>Grade Level</label>
       			<select class="form-control form-control-sm" id="e_gradelevel">
       				<option value="Kindergarten">Kindergarten</option>
       				<option value="Grade 1">Grade 1</option>
       			</select>
       		</div>
       	</div>
       	<div class="col-sm-3">
       		<div class="form-group">
       			<label>LRN</label>
       			<input type="text" id="e_lrn" class="form-control-sm form-control" name="">
       		</div>
       	</div>

       	<div class="col-sm-3">
       		<div class="form-group">
       			<br>
       			<label>Is Sped</label>
       			<input type="checkbox" id="e_issped" name="">
       		</div>
       	</div>
       	<div class="col-sm-12">
       		
       	</div>
   	 	<div class="col-sm-3">
       		<div class="form-group">
       			<label>Last Name</label>
       			<input type="text" id="e_lastname" class="form-control-sm form-control" name="">
       		</div>
       	</div>
       	 	<div class="col-sm-3">
       		<div class="form-group">
       			<label>First Name</label>
       			<input type="text" id="e_firstname" class="form-control-sm form-control" name="">
       		</div>
       	</div>
       	 	<div class="col-sm-3">
       		<div class="form-group">
       			<label>Middle Name</label>
       			<input type="text" id="e_middlename" class="form-control-sm form-control" name="">
       		</div>
       	</div>
       		<div class="col-sm-3">
       		<div class="form-group">
       			<label>Ext Name</label>
       			<input type="text" id="e_extname" class="form-control-sm form-control" name="">
       		</div>
       	</div>
       		<div class="col-sm-12">
       		
       	</div>
       	<div class="col-sm-4">
       		<div class="form-group">
   				<label>Gender</label><br>
       			<label><input type="radio" name="e_gender" id="e_gender_male"> Male</label>
       			<label><input type="radio" name="e_gender" id="e_gender_female"> Female</label>
       		</div>
       	</div>
       		<div class="col-sm-4">
       		<div class="form-group">
   				<label>Birthday</label><br>
       			<input type="date" class="form-control-sm form-control" id="e_birthdate" name="">
       		</div>
       	</div>

       	    	 	<div class="col-sm-2">
       		<div class="form-group">
       			<label>Age</label>
       			<input type="number" id="e_age" class="form-control-sm form-control" name="">
       		</div>
       	</div>
       	<div class="col-sm-5">
       		<div class="form-group">
       			<label>Birthplace</label>
       			<input type="text" id="e_birthplace" class="form-control-sm form-control" name="">
       		</div>
       	</div>
       </div>
      	  </div>
      	  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
      	  	<!-- CONTACT INFORMATION -->
      	  	<div class="row">
      	  		<div class="col-sm-3">
      	  			<div class="form-group">
      	  				<label>Street</label>
      	  				<input type="text" id="e_street" class="form-control form-control-sm" name="">
      	  			</div>
      	  		</div>
      	  		<div class="col-sm-3">
      	  			<div class="form-group">
      	  				<label>Barangay</label>
      	  				<input type="text" id="e_barangay" class="form-control form-control-sm" name="">
      	  			</div>
      	  		</div>
      	  		<div class="col-sm-3">
      	  			<div class="form-group">
      	  				<label>City / Municipality</label>
      	  				<input type="text" id="e_municipality" class="form-control form-control-sm" name="">
      	  			</div>
      	  		</div>
      	  		<div class="col-sm-4">
      	  			<div class="form-group">
      	  				<label>Fathers Name</label>
      	  				<input type="text" id="e_fathersname" class="form-control form-control-sm" name="">
      	  			</div>
      	  		</div>
      	  		<div class="col-sm-4">
      	  			<div class="form-group">
      	  				<label>Mothers Name</label>
      	  				<input type="text" id="e_mothersname" class="form-control form-control-sm" name="">
      	  			</div>
      	  		</div>
      	  		<div class="col-sm-3">
      	  			<div class="form-group">
      	  				<label>Contact Number</label>
      	  				<input type="text" id="e_parentscontact" class="form-control form-control-sm" name="">
      	  			</div>
      	  		</div>
      	  		<div class="col-sm-12">
      	  			<h6>If Guardian</h6>
      	  		</div>
      	  		<div class="col-sm-4">
      	  			<div class="form-group">
      	  				<label>Guardian Name</label>
      	  				<input type="text" id="e_guardian_name" class="form-control form-control-sm" name="">
      	  			</div>
      	  		</div>
      	  		<div class="col-sm-3">
      	  			<div class="form-group">
      	  				<label>Relationship</label>
      	  				<input type="text" class="form-control form-control-sm" id="e_guardianrelationship" name="">
      	  			</div>
      	  		</div>
      	  		<div class="col-sm-3">
      	  			<div class="form-group">
      	  				<label>Contact Number</label>
      	  				<input type="text" class="form-control form-control-sm" id="e_gruadiancontact" name="">
      	  			</div>
      	  		</div>
      	  	</div>
      	  </div>
      	  <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
      	  	<!-- PREVIOUS SCHOOL INFORMATION -->
      	  	<div class="row">
      	  		<div class="col-sm-9">
      	  			<div class="form-group">
      	  				<label>Previous School Name</label>
      	  				<input type="text" class="form-control form-control-sm" id="e_previous_school" name="">
      	  			</div>
      	  		</div>
      	  		<div class="col-sm-4">
      	  			<div class="form-group">
      	  				<label>Grade Level</label>
      	  				<select class="form-control form-control-sm" id="e_previous_gradelevel">
       					<option value="Kindergarten">Kindergarten</option>
       				<option value="Grade 1">Grade 1</option>
       			</select>
      	  			</div>
      	  		</div>
      	  		<div class="col-sm-4">
      	  			<div class="form-group">
      	  				<label>School Year</label>
      	  				<select class="form-control form-control-sm" id="e_previous_schoolyear">
       				<option value="2020-2021">2020-2021</option>
       				<option value="2019-2020">2019-2020</option>
       				<option value="2018-2019">2018-2019</option>
       				<option value="2017-2018">2017-2018</option>
       				<option value="2016-2017">2016-2017</option>
       				<option value="2015-2016">2015-2016</option>
       				<option value="2014-2015">2014-2015</option>
       				<option value="2013-2014">2013-2014</option>
       				<option value="2012-2013">2012-2013</option>
       				<option value="2011-2012">2011-2012</option>
       			</select>
      	  			</div>
      	  		</div>
      	  	</div>
      	  </div>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="save_edit()"><i class="far fa-save"></i> Save Modifications</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="far fa-times-circle"></i> Cancel</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	function openedit(contro_obj){
		$("#searchresmod").modal("hide");
		var eid = $(contro_obj).data("eid");
		$.ajax({
			type: "POST",
			url: "index.php",
			data: {tag:"request_data_info",data_id:eid},
			success: function (data){
				data = JSON.parse(data);

				$("#e_refcode").val(data["enrl_code"]);
				$("#e_gradelevel").val(data["level"]);
				$("#e_lrn").val(data["lrn"]);
				$("#e_lastname").val(data["last_name"]);
				$("#e_firstname").val(data["first_name"]);
				$("#e_middlename").val(data["middle_name"]);
				$("#e_extname").val(data["ext_name"]);

				if(data["sex"] == "0"){
					 $("#e_gender_female").prop("checked", true);
				}else{
					 $("#e_gender_male").prop("checked", true);
				}
				// alert(data["is_sped"]);
				if(data["is_sped"] == "0"){
					// alert("is not sped");
					 $("#e_issped").prop("checked", false);
				}else{
					// alert("is sped");
					 $("#e_issped").prop("checked", true);
				}

				$("#e_birthdate").val(data["birthday"]);
				$("#e_age").val(data["age"]);
				$("#e_birthplace").val(data["birth_place"]);

				// CONTACT INFO
				$("#e_street").val(data["address"]);
				$("#e_barangay").val(data["barangay"]);
				$("#e_municipality").val(data["city"]);

				$("#e_fathersname").val(data["fathers_name"]);
				$("#e_mothersname").val(data["mothers_name"]);
				$("#e_parentscontact").val(data["fcontact"]);

				$("#e_guardian_name").val(data["guardians_name"]);
				$("#e_guardianrelationship").val(data["guardians_relationship"]);
				$("#e_gruadiancontact").val(data["guardians_contact"]);

				//PREVIOUS SCHOOL INFO

				$("#e_previous_school").val(data["prevschool"]);
				$("#e_previous_gradelevel").val(data["prev_level"]);
				$("#e_previous_schoolyear").val(data["syattended"]);
			}
		})
	}

	function save_edit(){

	var e_gender = "0";
	var e_issped = "0";

	var e_refcode = $("#e_refcode").val(); 
	var e_gradelevel = $("#e_gradelevel").val();
	var e_lrn = $("#e_lrn").val();

	if($("#e_issped").prop("checked") == true){
		e_issped = "1";
	}

	var e_lastname = $("#e_lastname").val();
	var e_firstname = $("#e_firstname").val();
	var e_middlename = $("#e_middlename").val();
	var e_extname = $("#e_extname").val();

	if($("#e_gender_male").prop("checked") == true){
		e_gender = "1";
	}
	var e_birthdate = $("#e_birthdate").val();
	var e_age = $("#e_age").val();
	var e_birthplace = $("#e_birthplace").val();
	var e_street = $("#e_street").val();
	var e_barangay = $("#e_barangay").val();
	var e_municipality = $("#e_municipality").val();
	var e_fathersname = $("#e_fathersname").val();
	var e_mothersname = $("#e_mothersname").val();
	var e_parentscontact = $("#e_parentscontact").val();
	var e_guardian_name = $("#e_guardian_name").val();
	var e_guardianrelationship = $("#e_guardianrelationship").val();
	var e_gruadiancontact = $("#e_gruadiancontact").val();
	var e_previous_school = $("#e_previous_school").val();
	var e_previous_gradelevel = $("#e_previous_gradelevel").val();
	var e_previous_schoolyear = $("#e_previous_schoolyear").val();
$.ajax({
	type: "POST",
	url: "index.php",
	data: {tag:"save_edit_student",reference_code: e_refcode,
	lrn:e_lrn,
	studentgradelevel:e_gradelevel,
	lastname:e_lastname,
	firstname:e_firstname,
	middlename:e_middlename,
	extname:e_extname,
	gender:e_gender,
	age:e_age,
	birthdate:e_birthdate,
	birthplace:e_birthplace,
	previous_school:e_previous_school,
	school_year:e_previous_schoolyear,
	grade_level_previous:e_previous_gradelevel,
	street:e_street,
	barangay:e_barangay,
	municipality:e_municipality,
	fathersname:e_fathersname,
	mothersname:e_mothersname,
	contact_number_parents:e_parentscontact,
	guardiansname:e_guardian_name,
	guardiansrelationship:e_guardianrelationship,
	contact_number_guardians:e_gruadiancontact,
	is_sped:e_issped
},
	success: function(data){
		alert("Saved");
		location.reload();
	}
})
	}
</script>


	<form action="index.php" method="POST" target="_blank">
		<div class="modal" tabindex="-1" id="modal_csvstudents" role="dialog">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title">Export All Registered Students to Excel file</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		      	<input type="hidden" name="tag" value="print_reg_tocsv">
		        <p>Click "Export Excel" to download all registered students data as Excel file.</p>
		      </div>
		      <div class="modal-footer">
		        <button type="submit" class="btn btn-primary">Export Excel</button>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      </div>
		    </div>
		  </div>
		</div>
	</form>

	<div class="modal" tabindex="-1" id="searchresmod" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="searchtext"> Search Result for </h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body" style="min-height: 500px;">
			<div id="resultxsearch" >
				
			</div>
	      </div>
	    </div>
	  </div>
	</div>

	<script type="text/javascript">
CountSentSummary();
		function CountSentSummary(){
			$.ajax({
				type: "POST",
				url:"index.php",
				data: {tag: "getallsentinfocount"},
				success: function(data){
					$("#sentcountsumm").html(data);

					var simm = data.split("/");
					if(simm[0] == simm[1]){
						$("#pan_yes").css("display","block");
					}else{
						$("#pan_no").css("display","block");
					}
				}
			})
		}
		function easysearch(){
			var wordkey = $("#keywordinp").val();
				$("#resultxsearch").html("");
			if(wordkey != ""){
			
			$("#searchindicator").css("display","block");
			$.ajax({
					type:"POST",
					url: "index.php",
					data: {tag:"searchitlikeaman",searchkey:wordkey},
					success: function(data){
						setTimeout(function(){
							$("#searchresmod").modal("show");
			$("#searchtext").html("<i class='fas fa-search'></i> Search result for : " + wordkey);
$("#resultxsearch").html(data);
						$("#searchindicator").css("display","none");
					},500)
					}
				})
		}else{

		}
		}
	</script>
	<div id="mostrecent">
		<h5 class="mt-3">Most Recent</h5>
	<h6>Displaying only 64 max out of <span></span> overall</h6>
			<table class="table table-sm table-striped table-bordered" id="stu_allreg_recent">
			<thead>
				<tr>
					<th>#</th>
					<th>LRN</th>
				
					<th>Fullname</th>
					<th>Gender</th>
					<th>Age</th>
					<th>Birthday</th>
					<th>Barangay</th>
					<th>SPED</th>
					<th>Date Registered</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="tbl_allstudent_reg_recent">
				
			</tbody>
		</table>
	</div>
</div>

<div class="mt-3">
	<div id="panel_seeallconfirmation" style="display: block;">
		<div class="card">
			<div class="card-body">
				<h5>Do you want to see all registration data?</h5>
		<h6>This may take time to show. Click the "Show All" button to begin</h6>
		<button onclick="seeAll()" class="btn btn-primary btn-sm mt-4">Show All</button>
			</div>
		</div>
	</div>

	<div id="panel_loading" style="display: none;">
		<img src="images/processing.gif" style="width: 100px;">
		<h3>Preparing data...</h3>
	</div>
	<div id="panel_allreg" style="display: none;">
		 <h5>All Registered</h5>
	<h6>Displaying overall data</h6>
			<table class="table table-sm table-striped table-bordered" id="stu_allreg">
			<thead>
				<tr>
					<th>#</th>
					<th>LRN</th>
				
					<th>Fullname</th>
					<th>Gender</th>
					<th>Age</th>
					<th>Birthday</th>
					<th>Barangay</th>
					<th>SPED</th>
					<th>Date Registered</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="tbl_allstudent_reg">
				
			</tbody>
		</table>
	</div>
</div>

<div class="modal" tabindex="-1" id="modal_transferreg" role="dialog">
  <div class="modal-dialog" role="document">
  		   <div class="modal-content">
 <div id="hasstudents" style="display: none;">
 	<!-- HAS STUDENTS -->
 
      <div class="modal-header">
        <h5 class="modal-title">Send Registration Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Send all student registration record to SDO - Marikina Server?</p>
      </div>
      <div class="modal-footer">
        <a href="transferpage.php" class="btn btn-primary">Send</a>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>


 </div>
 <div id="hasnostudents" style="display: none;">
 	<!-- HAS NO STUDENTS -->

 
      <div class="modal-header">
        <h5 class="modal-title">Send Registration Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<h5>No data to send yet.</h5>
        <p>You currently have zero registered students yet.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>



 </div>
     </div>
  </div>
</div>


</body>
</html>

</div>
</body>
</html>
<script type="text/javascript">
	getRecent();
	function seeAll(){
		$("#panel_seeallconfirmation").css("display","none");
		$("#panel_loading").css("display","block");
		$.ajax({
		type:"POST",
		url: "index.php",
		data: {tag:"get_all_regstu_info"},
		success: function(data){
			$("#tbl_allstudent_reg").html(data);
			$("#stu_allreg").DataTable();
			$("#panel_loading").css("display","none");
			$("#panel_allreg").css("display","block");
			$("#mostrecent").css("display","none");

			alert("Load complete!");
		}
	})

	}
	function getRecent(){
		$.ajax({
		type:"POST",
		url: "index.php",
		data: {tag:"get_all_regstu_info_recent"},
		success: function(data){
			if(data == ""){
$("#hasstudents").css("display","none");
$("#hasnostudents").css("display","block");


			}else{
$("#hasstudents").css("display","block");
$("#hasnostudents").css("display","none");
			}
			$("#tbl_allstudent_reg_recent").html(data);
			$("#stu_allreg_recent").DataTable();
		}
	})

	}

</script>