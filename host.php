
<?php include("php/auth.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>OLA - SR</title>
	<?php
include("theme/original.php");
?>
</head>
<body>
		<?php
include("components/navbar.php");
?>
<div class="container-fluid">
<div class="row">
	<div class="col-lg-2">
		<!-- SIDEBAR -->
		<?php
include("components/sidebar.php");
?>
		<!-- SIDEBAR -->
	</div>
	<div class="col-lg-10">
				<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		  <a class="navbar-brand" href="#"> Dashboard</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
		        <!-- <a target="_blank" class="nav-link" href="http://depedmarikina.ph" data-toggle="modal" data-target="#new_reg_modal"><i class="far fa-user-circle"></i> Register New Encoder</span></a> -->
		      </li>
		    </ul>
		  </div>
		</nav>

		<div class="container-fluid" id="hasstudents">
			<!-- HAS STUDENT -->
			<h5 class="card-title mt-3">Registration Summary</h5>
				<div class="row">
					<div class="col-md-4" style="display: none;">
						
				<div class="card mt-3">
			<div class="card-body">
				<h5 class="card-title"><i class="far fa-check-circle"></i> Registered Learners</h5>
				<h1 id="countofallreg">45</h1>
			</div>
		</div>
					</div>
					<div class="col-md-4" style="display: none;">
						
				<div class="card mt-3">
			<div class="card-body">
				<h5 class="card-title"><i class="far fa-keyboard"></i> Encoder Accounts</h5>
				<h1 id="myenccount">45</h1>
			</div>
		</div>
					</div>





					<div class="col-lg-12">
						 <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
						  <li class="nav-item">
						    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true"><i class="fas fa-user-check"></i> Regular</a>
						  </li>
						  <li class="nav-item">
						    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false"><i class="fas fa-star"></i> SPED</a>
						  </li>
						</ul>
						<div class="tab-content" id="pills-tabContent">
						  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">

<table class="table mt-3 table-bordered table-striped table-sm">
					<thead>
						<tr>
							<th colspan="5" style="border-color: #f6b93b; background-color: #fad390; text-align: center;">Kinder</th>
							<th colspan="5" style="border-color: rgba(7, 153, 146,1.0); background-color: rgba(56, 173, 169,1.0); text-align: center;">Grade 1</th>
						</tr>
						<tr>
							<th colspan="2" style="background-color: rgba(116, 185, 255,0.5);"><center><i class="fas fa-mars"></i> Male</center></th>
							<th colspan="2" style="background-color: rgba(253, 121, 168,0.5);"><center><i class="fas fa-venus"></i> Female</center></th>
							<th rowspan="2" style="background-color: #fad390;"><center>Total</center></th>
							<th colspan="2" style="background-color: rgba(116, 185, 255,0.5);"><center><i class="fas fa-mars"></i> Male</center></th>
							<th colspan="2" style="background-color:  rgba(253, 121, 168,0.5);"><center><i class="fas fa-venus"></i> Female</center></th>
							<th rowspan="2" style="background-color:  rgba(56, 173, 169,1.0);"><center>Total</center></th>
						</tr>
						<tr>
								<th style="background-color: rgba(116, 185, 255,0.5);" ><center>Count</center></th>
							<th style="background-color: rgba(116, 185, 255,0.5);" ><center>%</center></th>
							<th style="background-color: rgba(253, 121, 168,0.5);" ><center>Count</center></th>
							<th style="background-color: rgba(253, 121, 168,0.5);" ><center>%</center></th>

							<th style="background-color: rgba(116, 185, 255,0.5);" ><center>Count</center></th>
							<th style="background-color: rgba(116, 185, 255,0.5);" ><center>%</center></th>
							<th style="background-color: rgba(253, 121, 168,0.5);" ><center>Count</center></th>
							<th style="background-color: rgba(253, 121, 168,0.5);" ><center>%</center></th>
						</tr>
					</thead>
					<tbody id="tbl_sum_count">
						
					</tbody>
				</table>

				<table class="table mt-3 table-bordered table-striped table-sm">
					<thead>
						<thead>
							<tr>
								<th rowspan="3" style="background-color: rgba(95, 39, 205,0.5);"><center>Age</center></th>
								<th colspan="5" style="border-color: #f6b93b; background-color: #fad390;"><center>Kinder</center></th>
								<th colspan="5" style="border-color: rgba(7, 153, 146,1.0); background-color: rgba(56, 173, 169,1.0);"><center>Grade 1</center></th>
							</tr>
							<tr>
								<th colspan="2" style="background-color:rgba(116, 185, 255,0.5);"><center><i class="fas fa-mars"></i> Male</center></th>
								<th colspan="2" style="background-color:rgba(253, 121, 168,0.5);"><center><i class="fas fa-venus"></i> Female</center></th>
								<th rowspan="2" style="background-color: #fad390;"><center>Total</center></th>

								<th colspan="2" style="background-color: rgba(116, 185, 255,0.5);"><center><i class="fas fa-mars"></i> Male</center></th>
								<th colspan="2" style="background-color: rgba(253, 121, 168,0.5);"><center><i class="fas fa-venus"></i> Female</center></th>
								<th rowspan="2" style="background-color: rgba(56, 173, 169,1.0);"><center>Total</center></th>
							</tr>
							<tr>
							<th style="background-color: rgba(116, 185, 255,0.5);" ><center>Count</center></th>
							<th style="background-color: rgba(116, 185, 255,0.5);" ><center>%</center></th>
							<th style="background-color: rgba(253, 121, 168,0.5);" ><center>Count</center></th>
							<th style="background-color: rgba(253, 121, 168,0.5);" ><center>%</center></th>

							<th style="background-color: rgba(116, 185, 255,0.5);" ><center>Count</center></th>
							<th style="background-color: rgba(116, 185, 255,0.5);" ><center>%</center></th>
							<th style="background-color: rgba(253, 121, 168,0.5);" ><center>Count</center></th>
							<th style="background-color: rgba(253, 121, 168,0.5);" ><center>%</center></th>
							</tr>
						</thead>
					</thead>
					<tbody id="tbl_sum_age">
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>

							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>

						  </div>
						  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
						  	
	<table class="table mt-3 table-bordered table-striped table-sm">
					<thead>
						<tr>
							<th colspan="5" style="border-color: #f6b93b; background-color: #fad390; text-align: center;">Kinder</th>
							<th colspan="5" style="border-color: rgba(7, 153, 146,1.0); background-color: rgba(56, 173, 169,1.0); text-align: center;">Grade 1</th>
						</tr>
						<tr>
							<th colspan="2" style="background-color: rgba(116, 185, 255,0.5);"><center><i class="fas fa-mars"></i> Male</center></th>
							<th colspan="2" style="background-color: rgba(253, 121, 168,0.5);"><center><i class="fas fa-venus"></i> Female</center></th>
							<th rowspan="2" style="background-color: #fad390;"><center>Total</center></th>
							<th colspan="2" style="background-color: rgba(116, 185, 255,0.5);"><center><i class="fas fa-mars"></i> Male</center></th>
							<th colspan="2" style="background-color:  rgba(253, 121, 168,0.5);"><center><i class="fas fa-venus"></i> Female</center></th>
							<th rowspan="2" style="background-color:  rgba(56, 173, 169,1.0);"><center>Total</center></th>
						</tr>
						<tr>
								<th style="background-color: rgba(116, 185, 255,0.5);" ><center>Count</center></th>
							<th style="background-color: rgba(116, 185, 255,0.5);" ><center>%</center></th>
							<th style="background-color: rgba(253, 121, 168,0.5);" ><center>Count</center></th>
							<th style="background-color: rgba(253, 121, 168,0.5);" ><center>%</center></th>

							<th style="background-color: rgba(116, 185, 255,0.5);" ><center>Count</center></th>
							<th style="background-color: rgba(116, 185, 255,0.5);" ><center>%</center></th>
							<th style="background-color: rgba(253, 121, 168,0.5);" ><center>Count</center></th>
							<th style="background-color: rgba(253, 121, 168,0.5);" ><center>%</center></th>
						</tr>
					</thead>
					<tbody id="sped_tbl_sum_count">
						
					</tbody>
				</table>
			<table class="table mt-3 table-bordered table-striped table-sm">
					<thead>
						<thead>
							<tr>
								<th rowspan="3" style="background-color: rgba(95, 39, 205,0.5);"><center>Age</center></th>
								<th colspan="5" style="border-color: #f6b93b; background-color: #fad390;"><center>Kinder</center></th>
								<th colspan="5" style="border-color: rgba(7, 153, 146,1.0); background-color: rgba(56, 173, 169,1.0);"><center>Grade 1</center></th>
							</tr>
							<tr>
								<th colspan="2" style="background-color:rgba(116, 185, 255,0.5);"><center><i class="fas fa-mars"></i> Male</center></th>
								<th colspan="2" style="background-color:rgba(253, 121, 168,0.5);"><center><i class="fas fa-venus"></i> Female</center></th>
								<th rowspan="2" style="background-color: #fad390;"><center>Total</center></th>

								<th colspan="2" style="background-color: rgba(116, 185, 255,0.5);"><center><i class="fas fa-mars"></i> Male</center></th>
								<th colspan="2" style="background-color: rgba(253, 121, 168,0.5);"><center><i class="fas fa-venus"></i> Female</center></th>
								<th rowspan="2" style="background-color: rgba(56, 173, 169,1.0);"><center>Total</center></th>
							</tr>
							<tr>
							<th style="background-color: rgba(116, 185, 255,0.5);" ><center>Count</center></th>
							<th style="background-color: rgba(116, 185, 255,0.5);" ><center>%</center></th>
							<th style="background-color: rgba(253, 121, 168,0.5);" ><center>Count</center></th>
							<th style="background-color: rgba(253, 121, 168,0.5);" ><center>%</center></th>

							<th style="background-color: rgba(116, 185, 255,0.5);" ><center>Count</center></th>
							<th style="background-color: rgba(116, 185, 255,0.5);" ><center>%</center></th>
							<th style="background-color: rgba(253, 121, 168,0.5);" ><center>Count</center></th>
							<th style="background-color: rgba(253, 121, 168,0.5);" ><center>%</center></th>
							</tr>
						</thead>
					</thead>
					<tbody id="sped_tbl_sum_age">
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>

							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>

						  </div>
						</div>
					</div>
	</div>
</div>


		<div class="container-fluid" id="hasnostudents">
			<!-- HAS NO STUDENT -->
			<h1 class="mt-5">You have no registered students yet!</h1>
	<h6>Summary of registered students will appear here.</h6>
		</div>


</div>
</div>
</body>
</html>



<script type="text/javascript">
GetItHasRegisteredStudents();
	function GetItHasRegisteredStudents(){
		$.ajax({
			type: "POST",
			url: "index.php",
			data: {tag: "getifhasregisteredstudentnow"},
			success:function(data){
					if(data == "false"){
						$("#hasnostudents").css("display","block");
						$("#hasstudents").css("display","none");
					}else{
						$("#hasnostudents").css("display","none");
						$("#hasstudents").css("display","block");

			getsumbycount();
			getsumbyage();
			getallregisteredcount();
			getallregisteredencoderaccounts();
			sped_getsumbycount();
			sped_getsumbyage();
					}
			}
		})
	}

	function getallregisteredcount(){
		
				$.ajax({
		type :"POST",
		url: "index.php",
		data: {tag: "countofallreg"},
		success: function(data){
				$("#countofallreg").html(data);
		}
	})
	}
	function getsumbycount(){
		$.ajax({
		type :"POST",
		url: "index.php",
		data: {tag: "getsumbycount"},
		success: function(data){
				$("#tbl_sum_count").html(data);
		}
	})
	}
	function getsumbyage(){
		$.ajax({
		type :"POST",
		url: "index.php",
		data: {tag: "getsumbyage"},
		success: function(data){
		$("#tbl_sum_age").html(data);
		}
		})
	}


	function sped_getsumbycount(){
		$.ajax({
		type :"POST",
		url: "index.php",
		data: {tag: "getsumbycount_sped"},
		success: function(data){
				$("#sped_tbl_sum_count").html(data);
		}
	})
	}
	function sped_getsumbyage(){
		$.ajax({
		type :"POST",
		url: "index.php",
		data: {tag: "getsumbyage_sped"},
		success: function(data){
		$("#sped_tbl_sum_age").html(data);
		}
		})
	}
	function getallregisteredencoderaccounts(){
$.ajax({
		type :"POST",
		url: "index.php",
		data: {tag: "get_reg_enc"},
		success: function(data){
		$("#myenccount").html(data);
		}
		})
	}
</script>