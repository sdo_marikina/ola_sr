<!DOCTYPE html>
<html>
<head>
	<title>Print Registration Info</title>
</head>
<body>
<?php
include("theme/original.php");
$c = mysqli_connect("localhost","root","","ola_sr");
mysqli_set_charset($c,"utf8");
date_default_timezone_set("Asia/Manila");
$q = "SELECT * FROM registered_students LEFT JOIN station_info ON registered_students.school = station_info.station_id WHERE registered_students.id='" . $_GET["my_reg_id"] . "' LIMIT 1";
$res = mysqli_query($c,$q);
$row = mysqli_fetch_all($res,MYSQLI_ASSOC);

?>
<div class="container">
	<h4 class="float-right text-muted">Reference Code:<br><u><?php echo $row[0]["enrl_code"]; ?></u></h4>
	
	<h5 class="card-title mt-5"><strong><?php echo $row[0]["full_station_name"]; ?></strong><br>Student Registration Info</h5>
	<h6 class="card-subtitle">Registration Date: <?php echo date("F d, Y", strtotime($row[0]["date_register"])); ?></h6>

	<h6 class="card-subtitle mt-3 text-muted">Personal Information</h6>
	<table class="table table-sm table-bordered table-striped mt-3">
		<tbody>
			<tr>
				<th>LRN</th>
				<td><?php
					if($row[0]["lrn"] == "0"){
						 echo "<span class='text-muted'>No LRN</span>";
					}else{
						 echo $row[0]["lrn"];
					}
				 ?></td>
			</tr>
			<tr>
				<th>Fullname</th>
				<td><?php echo $row[0]["last_name"] . ", " .  $row[0]["first_name"] . " " .  $row[0]["middle_name"] . " " . $row[0]["ext_name"]; ?></td>
			</tr>
			<tr>
				<th>Gender</th>
				<td><?php 
					if($row[0]["sex"] == "1"){
						echo "Male";
					}else{
						echo "Female";
					}
				 ?></td>
			</tr>
			<tr>
				<th>Age</th>
				<td><?php 
					echo $row[0]["age"] . " years old";
				 ?></td>
			</tr>
			<tr>
				<th>Birthdate</th>
				<td><?php 
					echo date("F d, Y",strtotime($row[0]["birthday"]));
				 ?></td>
			</tr>
			<tr>
				<th>Birthplace</th>
				<td><?php 
					echo $row[0]["birth_place"];
				 ?></td>
			</tr>
			<tr>
				<th>Level</th>
				<td><?php 

				if($row[0]["is_sped"] == "1"){
					echo "<small class='text-muted float-right'>REGISTERED AS SPED</small>";
				}
					echo $row[0]["level"];
				 ?></td>
			</tr>
		</tbody>
	</table>

	<h6 class="card-subtitle mt-3 text-muted">Contact Information</h6>
	<table class="table table-sm table-bordered table-striped mt-3">
		<tbody>
			<tr>
				<th>Street</th>
				<td><?php 
				// echo $row[0]["address"];

				if($row[0]["address"] == ""){
						echo "<small class='text-muted'>None Provided</small>";
					}else{
						echo $row[0]["address"];
					} ?></td>
			</tr>
			<tr>
				<th>Barangay</th>
				<td><?php 
				// echo $row[0]["barangay"];

				if($row[0]["barangay"] == ""){
						echo "<small class='text-muted'>None Provided</small>";
					}else{
						echo $row[0]["barangay"];
					} ?></td>
			</tr>
			<tr>
				<th>Municipality / City</th>
				<td><?php 
				// echo $row[0]["city"];

				if($row[0]["city"] == ""){
						echo "<small class='text-muted'>None Provided</small>";
					}else{
						echo $row[0]["city"];
					} ?></td>
			</tr>
			<tr>
				<th>Father's Name</th>
				<td><?php 
				// echo $row[0]["fathers_name"];

				if($row[0]["fathers_name"] == ""){
						echo "<small class='text-muted'>None Provided</small>";
					}else{
						echo $row[0]["fathers_name"];
					} ?></td>
			</tr>
			<tr>
				<th>Mother's Name</th>
				<td><?php 
				// echo $row[0]["mothers_name"];

				if($row[0]["mothers_name"] == ""){
						echo "<small class='text-muted'>None Provided</small>";
					}else{
						echo $row[0]["mothers_name"];
					} ?></td>
			</tr>
			<tr>
				<th>Contact Number</th>
				<td><?php 
				// echo $row[0]["mcontact"];

				if($row[0]["mcontact"] == ""){
						echo "<small class='text-muted'>None Provided</small>";
					}else{
						echo $row[0]["mcontact"];
					} ?></td>
			</tr>
			<tr>
				<th colspan="2"><center>Guardian Information</center></th>
			</tr>
			<tr>
				<th>Guardians's Name</th>
				<td><?php 
				// echo $row[0]["guardians_name"];

				if($row[0]["guardians_name"] == ""){
						echo "<small class='text-muted'>None Provided</small>";
					}else{
						echo $row[0]["guardians_name"];
					} ?></td>
			</tr>
			<tr>
				<th>Relationship</th>
				<td><?php 
				// echo $row[0]["guardians_relationship"];

				if($row[0]["guardians_relationship"] == ""){
						echo "<small class='text-muted'>None Provided</small>";
					}else{
						echo $row[0]["guardians_relationship"];
					} ?></td>
			</tr>
			<tr>
				<th>Contact Number</th>
				<td><?php 
				// echo $row[0]["guardians_contact"];

				if($row[0]["guardians_contact"] == ""){
						echo "<small class='text-muted'>None Provided</small>";
					}else{
						echo $row[0]["guardians_contact"];
					} ?></td>
			</tr>
		</tbody>
	</table>

	<h6 class="card-subtitle mt-3 text-muted">Previous School Information</h6>
	<table class="table table-sm table-bordered table-striped mt-3">
		<tbody>
			<tr>
				<th>Previous School Information</th>
				<td><?php 

				if($row[0]["prevschool"] == ""){
						echo "<small class='text-muted'>None Provided</small>";
					}else{
						echo $row[0]["prevschool"];
					} ?></td>
			</tr>
<tr>
				<th>Grade Level</th>
				<td><?php 

				if($row[0]["prev_level"] == ""){
						echo "<small class='text-muted'>None Provided</small>";
					}else{
						echo $row[0]["prev_level"];
					} ?></td>
			</tr>
			<tr>
				<th>School Year</th>
				<td><?php 
					if($row[0]["syattended"] == ""){
						echo "<small class='text-muted'>None Provided</small>";
					}else{
						echo $row[0]["syattended"];
					}
				 ?></td>
			</tr>
		</tbody>
	</table>
	<hr>
	<small>Date Printed : <?php echo date("Y-m-d g:i a"); ?></small>
</div>

</body>
</html>